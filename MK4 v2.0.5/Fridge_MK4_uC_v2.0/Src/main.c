/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "canManager.h"
#include "flashManager.h"
#include "adcManager.h"
#include "i2cManager.h"
#include "math.h"
#include "stdlib.h"

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
CAN_HandleTypeDef hcan1;
I2C_HandleTypeDef hi2c1;

//###############  Multiple threads structure control  ###############
osThreadId mainTHandle;
osThreadId adcHandle;
osThreadId canTxHandle;
osThreadId canRxHandle;
osThreadId otaHandle;
osThreadId heaterHandle;
osMutexId canTxMutexHandle;
osMutexId canRxMutexHandle;
//###############  Microseconds timer configuration  ###############
TIM_HandleTypeDef usecondsTimHandle;
//###############  PCB configuration  ###############
uint8_t safeRestart = 0;
uint32_t heartBeatCounter = 0, HalfSecondTimer = 0;
//###############  CAN Communications control  ###############
uint8_t canTxBuffer [CAN_BUFFER_LENGTH] [CAN_MSG_LENGTH];
uint8_t canRxBuffer [CAN_BUFFER_LENGTH] [CAN_MSG_LENGTH];
uint8_t canTxBufferItems = 0, canRxBufferItems = 0;
uint8_t canSendAvailable = 0, canReceivedMessage = 0, canTxThreadConfingured = 0;
uint32_t synchronizationTimer = 0;
//###############  Memory control  ###############
FLASH_OBProgramInitTypeDef    OBInit;
FLASH_AdvOBProgramInitTypeDef AdvOBInit;
uint8_t flashBank = 0, OTArunning = 0, OTAnewData = 0;
uint32_t OTAtimeCounter = 0;
uint8_t checksum[WORD_BYTES];
uint8_t otaReceivedMessage[CAN_MSG_LENGTH];
//###############  ADCs control  ###############
uint32_t capSensorValue[CAP_SENSORS_NUMBER];
float sensorValues[ADC_NUMBER_SENSORS], AdcMovingAvg[ADC_NUMBER_SENSORS][MOVING_AVG_SAMPLES];
uint8_t humSensorConfigured = 0, capSensorConfigured = 0;
uint32_t humSensorTimer = 0, humSensorTempValue = 0, humSensorRelHumValue = 0, capSensorTimer = 0, tempWatchdog = 0;
//###############  Interruption process control  ###############
uint32_t initialDelayTimer = 0;
uint8_t StartInterruption = 0;
//###############  Fridge configuration  ###############
float fridgeTargetTemp = 4.50, averageFridgeTemp = 0, fridgeTemperatureDifferential = 2.8;
uint32_t fridgeCompressorOffTimer = 0, defrostRunningTime = 0, defrostCycleTimer = 0, decreaseTempButtonTimer = 0, defrostButtonTimer = 0, increaseTempButtonTimer = 0;
uint8_t fridgeMode = COOLING_MODE, fridgeStatus = 0, fridgeLockStatus = LOCK_FRIDGE, setCompressorOn = 0;
//###############  Defrost Heater configuration  ###############
uint8_t heaterZC = 0, heaterRun = 0;
uint32_t heaterTargetTemp = 0, DH_ZCcounter = 0;
uint8_t DH_PowerMode = 0, DH_TargetPowerMode = 10;
const uint8_t DH_POWER_MATRIX [DH_MAX_POWER_MODE+1][DH_CTRL_CYCLES] = {
		{0,0,0,0,0,0,0,0,0,0}, //CH_POWER_MODE_0
		{1,0,0,0,0,0,0,0,0,0}, //CH_POWER_MODE_1
		{1,0,1,0,0,0,0,0,0,0}, //CH_POWER_MODE_2
		{1,0,1,0,1,0,0,0,0,0}, //CH_POWER_MODE_3
		{1,0,1,0,1,0,1,0,0,0}, //CH_POWER_MODE_4
		{1,0,1,0,1,0,1,0,1,0}, //CH_POWER_MODE_5
		{1,1,1,0,1,0,1,0,1,0}, //CH_POWER_MODE_6
		{1,1,1,1,1,0,1,0,1,0}, //CH_POWER_MODE_7
		{1,1,1,1,1,1,1,0,1,0}, //CH_POWER_MODE_8
		{1,1,1,1,1,1,1,1,1,0}, //CH_POWER_MODE_9
		{1,1,1,1,1,1,1,1,1,1}, //CH_POWER_MODE_10
};
//###############  Humidity Sensor configuration  ###############
uint8_t humSensorSeqRead = 0;
//###############  Recirculation fan control  ###############
uint32_t RecircFan_DutyCycle = 0, RecircFan_TargetDutyCycle = 0;
TIM_HandleTypeDef RecircFan_TimerHandle;
TIM_OC_InitTypeDef RecircFan_TimerConfig;
uint32_t RecircFan_Period = 0, RecircFan_Pulse = 0, RecircFan_Timer = 0;


/* Private function prototypes -----------------------------------------------*/

//###############  System configuration  ###############
void SystemClock_Config(void);
static void GPIO_Config(void);
static void MicrosecondsTimer_Config(void);
static void ReadCurrentBank(void);
static void ChangeBootingBank(void);
//###############  Multiple threads  ###############
void MainTask(void const * argument);
void AdcSenseTask(void const * argument);
void CanTxTask(void const * argument);
void CanRxTask(void const * argument);
void FirmwareUpdateTask(void const * argument);
void HeaterTask(void const * argument);
//###############  CAN communication  ###############
static uint8_t addItem2CanTxBuffer(uint8_t newItem[CAN_MSG_LENGTH]);
static uint8_t sendNextItemFromCanTxBuffer(void);
static void canBufferTxReset(void);
static uint8_t addItem2CanRxBuffer(void);
static uint8_t readNextItemFromCanRxBuffer(void);
static void canBufferRxReset(void);
//###############  Recirculation fan control  ###############
static uint8_t RecircFan_PWM_Config(void);
static uint8_t RecircFan_DutyCycle_Config (uint32_t dc);

/* Public functions ---------------------------------------------------------*/
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void){
	/* MCU Configuration--------------------------------------------------------*/
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();
	/* Configure the system clock */
	SystemClock_Config();
	/* Initialize GPIO peripherals */
	GPIO_Config();
	/* Initialize timer peripherals */
	MicrosecondsTimer_Config();
	/* Initialize CAN peripherals */
	//TO UNCOMMENT
	if(CAN_Config()!=0){ ErrorHandler(CAN_CONFIG_ERROR_CMD);}
	/* Read in which bank we're running the code right now*/
	ReadCurrentBank();
	/* Initialize all configured peripherals with memory to peripherals communication or vice versa*/
	if(ADCSensors_Config()!=0){ ErrorHandler(CONFIG_ERROR_CMD);}
	adcStartDMA();
	/* Initialize I2C peripherals */
	if(i2cConfig() != 0){ ErrorHandler(I2C_ERROR_CMD);}
	if(RecircFan_PWM_Config() != 0){ ErrorHandler(CONFIG_ERROR_CMD);}

	/* Create the mutex(es) */
	osMutexDef(canTxMutex);
	canTxMutexHandle = osMutexCreate(osMutex(canTxMutex));
	//
	osMutexDef(canRxMutex);
	canRxMutexHandle = osMutexCreate(osMutex(canRxMutex));

	/* Create the thread(s) */
	osThreadDef(mainT, MainTask, osPriorityIdle, 0, 128);
	mainTHandle = osThreadCreate(osThread(mainT), NULL);
	//
	osThreadDef(adc, AdcSenseTask, osPriorityIdle, 0, 128);
	adcHandle = osThreadCreate(osThread(adc), NULL);
	//
	osThreadDef(canTx, CanTxTask, osPriorityIdle, 0, 128);
	canTxHandle = osThreadCreate(osThread(canTx), NULL);
	//
	osThreadDef(canRx, CanRxTask, osPriorityIdle, 0, 128);
	canRxHandle = osThreadCreate(osThread(canRx), NULL);
	//
	osThreadDef(ota, FirmwareUpdateTask, osPriorityIdle, 0, 128);
	otaHandle = osThreadCreate(osThread(ota), NULL);
	//
	osThreadDef(heater, HeaterTask, osPriorityLow, 0, 128);
	heaterHandle = osThreadCreate(osThread(heater), NULL);

	/* Start scheduler */
	osKernelStart();

	while (1){	}
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void){
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Configure the main internal regulator output voltage */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/** Initializes the CPU, AHB and APB busses clocks */
	RCC_OscInitStruct.OscillatorType 	= RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState 			= RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState 		= RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource 	= RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM 			= 25;
	RCC_OscInitStruct.PLL.PLLN 			= 336;
	RCC_OscInitStruct.PLL.PLLP 			= RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ 			= 7;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) { ErrorHandler(CONFIG_ERROR_CMD);}

	/** Initializes the CPU, AHB and APB busses clocks */
	RCC_ClkInitStruct.ClockType 		= RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource 		= RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider 	= RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider 	= RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider 	= RCC_HCLK_DIV2;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) { ErrorHandler(CONFIG_ERROR_CMD);}

	/** Enables the Clock Security System */
	HAL_RCC_EnableCSS();
}

/**
  * Microseconds timer initialization
  * Parameters: None
  * Return value: None
  */
static void MicrosecondsTimer_Config(void){
	/*##-1- Configure the TIM peripheral #######################################*/
	usecondsTimHandle.Instance 				= US_TIMER;
	usecondsTimHandle.Init.Period 			= (uint32_t) ((SystemCoreClock /2) / 100000) - 1;
	usecondsTimHandle.Init.Prescaler 		= 0;
	usecondsTimHandle.Init.ClockDivision 	= 0;
	usecondsTimHandle.Init.CounterMode 		= TIM_COUNTERMODE_UP;
	if(HAL_TIM_Base_Init(&usecondsTimHandle) != HAL_OK) { ErrorHandler(CONFIG_ERROR_CMD);}
	/*##-2- Start the TIM Base generation in interrupt mode ####################*/
	if(HAL_TIM_Base_Start_IT(&usecondsTimHandle) != HAL_OK) { ErrorHandler(CONFIG_ERROR_CMD);}
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void GPIO_Config(void){
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	//1#--GPIO Ports Clock Enable  ############################################
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	//2#--Pin Output Reset Status #############################################
	//Debugging LED
	HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);
	//Digital control devices
	HAL_GPIO_WritePin(DOOR_HEATER_EN_GPIO_Port, DOOR_HEATER_EN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(COOLING_FAN_GPIO_Port, COOLING_FAN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LIGHTS_EN_GPIO_Port, LIGHTS_EN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(COMPRESSOR_EN_GPIO_Port, COMPRESSOR_EN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(HEATER_EN_GPIO_Port, HEATER_EN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(ADD_AC_RELAY_1_EN_GPIO_Port, ADD_AC_RELAY_1_EN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(ADD_AC_RELAY_2_EN_GPIO_Port, ADD_AC_RELAY_2_EN_Pin, GPIO_PIN_RESET);

	//###############  Digital outputs  ###############
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	//Debug LED
	GPIO_InitStruct.Pin = DEBUG_LED_Pin;
	HAL_GPIO_Init(DEBUG_LED_GPIO_Port, &GPIO_InitStruct);
	//
	//Digital control devices
	GPIO_InitStruct.Pin = DOOR_HEATER_EN_Pin;
	HAL_GPIO_Init(DOOR_HEATER_EN_GPIO_Port, &GPIO_InitStruct);
	//
	GPIO_InitStruct.Pin = COOLING_FAN_Pin;
	HAL_GPIO_Init(COOLING_FAN_GPIO_Port, &GPIO_InitStruct);
	//
	GPIO_InitStruct.Pin = LIGHTS_EN_Pin;
	HAL_GPIO_Init(LIGHTS_EN_GPIO_Port, &GPIO_InitStruct);
	//
	GPIO_InitStruct.Pin = COMPRESSOR_EN_Pin;
	HAL_GPIO_Init(COMPRESSOR_EN_GPIO_Port, &GPIO_InitStruct);
	//
	GPIO_InitStruct.Pin = HEATER_EN_Pin;
	HAL_GPIO_Init(HEATER_EN_GPIO_Port, &GPIO_InitStruct);
	//
	GPIO_InitStruct.Pin = ADD_AC_RELAY_1_EN_Pin;
	HAL_GPIO_Init(ADD_AC_RELAY_1_EN_GPIO_Port, &GPIO_InitStruct);
	//
	GPIO_InitStruct.Pin = ADD_AC_RELAY_2_EN_Pin;
	HAL_GPIO_Init(ADD_AC_RELAY_2_EN_GPIO_Port, &GPIO_InitStruct);

	//###############  Digital input with rising interruption  ###############
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	//
	GPIO_InitStruct.Pin = AC_ZC_DETECT_Pin;
	HAL_GPIO_Init(AC_ZC_DETECT_GPIO_Port, &GPIO_InitStruct);

	//###############  Digital input with rising and falling interruption  ###############
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Pin = DECREASE_SET_TEMP_BTN_Pin;
	HAL_GPIO_Init(INCREASE_SET_TEMP_BTN_GPIO_Port, &GPIO_InitStruct);
	//
	GPIO_InitStruct.Pin = DEFROST_BTN_Pin;
	HAL_GPIO_Init(DEFROST_BTN_GPIO_Port, &GPIO_InitStruct);
	//
	GPIO_InitStruct.Pin = INCREASE_SET_TEMP_BTN_Pin;
	HAL_GPIO_Init(INCREASE_SET_TEMP_BTN_GPIO_Port, &GPIO_InitStruct);

	//###############  Interruption priority configuration  ###############
	HAL_NVIC_SetPriority(AC_ZC_DETECT_IRQn, 5, 4);
	HAL_NVIC_EnableIRQ(AC_ZC_DETECT_IRQn);
	//
	HAL_NVIC_SetPriority(DEFROST_BTN_IRQn, 6, 4);
	HAL_NVIC_EnableIRQ(DEFROST_BTN_IRQn);
	//
	HAL_NVIC_SetPriority(INCREASE_SET_TEMP_BTN_IRQn, 6, 5);
	HAL_NVIC_EnableIRQ(INCREASE_SET_TEMP_BTN_IRQn);
	//
	HAL_NVIC_SetPriority(DECREASE_SET_TEMP_BTN_IRQn, 6, 6);
	HAL_NVIC_EnableIRQ(DECREASE_SET_TEMP_BTN_IRQn);
}


/*  Main thread function, continuous check of VAT status during dispense, periodical safety checks and pump duty control
 * 	Parameters: constant argument not used
 * 	No Return:
 */
void MainTask(void const * argument){
	uint8_t lcdConfigured = 0, tempDefrostTriggered = 0;
	float oldEvaporatorTemp = 0.0;
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);

	while(1) {
//###- Periodical checks
		if(HalfSecondTimer > 500){
			HalfSecondTimer = 0;
			//Check restart request
			if(safeRestart == 1){
				osDelay(500);
				HAL_NVIC_SystemReset();
			}

			//Check interruption timer
			if((initialDelayTimer > 2000) && (StartInterruption == 0)){
				StartInterruption = 1;
			}

			//Display new changing items in the screen
			if(lcdConfigured == 0){
				if(lcdConfig() == 0){ lcdConfigured = 1;}
			}
			else if(lcdConfigured == 1){
				lcdDisplayTemp((uint32_t)(fridgeTargetTemp*100), averageFridgeTemp, fridgeMode, fridgeLockStatus, fridgeStatus);
			}

			//Configure humidity sensor
//			if(humSensorConfigured == 0){
//				if(humiditySensorConfig() == 0){
//					humSensorConfigured = 1;
//				}
//			}

			//Configure frost detection sensor
//			if(capSensorConfigured == 0){
//				if(frostSensorConfig() == 0){ capSensorConfigured = 1;}
//			}

			//Lock/Unlock Fridge buttons
			if((increaseTempButtonTimer > 3000) && (decreaseTempButtonTimer > 3000) && (HAL_GPIO_ReadPin(DECREASE_SET_TEMP_BTN_GPIO_Port, DECREASE_SET_TEMP_BTN_Pin) == GPIO_PIN_SET)
					&& (HAL_GPIO_ReadPin(INCREASE_SET_TEMP_BTN_GPIO_Port, INCREASE_SET_TEMP_BTN_Pin) == GPIO_PIN_SET)){
				if(fridgeLockStatus == LOCK_FRIDGE)
					fridgeLockStatus = UNLOCK_FRIDGE;
				else
					fridgeLockStatus = LOCK_FRIDGE;
				decreaseTempButtonTimer = increaseTempButtonTimer = 0;
			}

			//Check the restart reason of the microcontroller
			if(canTxThreadConfingured == 1){
				if((((RCC->CSR) & (1 << 29)) || ((RCC->CSR) & (1 << 28)) || ((RCC->CSR) & (1 << 25)) || ((RCC->CSR) & (1 << 30))) != 0){
					if(((RCC->CSR) & (1 << 29)) != 0){ sendResponse(PCB_NOTIFICATION_CMD, FRIDGE_RESET_CMD, IWDG_RESET_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);}
					else if(((RCC->CSR) & (1 << 28)) != 0){ sendResponse(PCB_NOTIFICATION_CMD, FRIDGE_RESET_CMD, SW_RESET_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);}
					else if(((RCC->CSR) & (1 << 25)) != 0){ sendResponse(PCB_NOTIFICATION_CMD, FRIDGE_RESET_CMD, BOR_RESET_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);}
					else{ sendResponse(PCB_NOTIFICATION_CMD, FRIDGE_RESET_CMD, WWDG_RESET_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);}
					//clear reset flags setting RMVF to 1
					RCC->CSR |= 1 << 24;
				}
				canTxThreadConfingured = 2;
			}

			//check temperature differentials
			if(tempWatchdog > 5000){
				if(abs(sensorValues[RIGHT_WALL_TEMP_ID] - sensorValues[LEFT_WALL_TEMP_ID]) > 7){
					sendResponse(PCB_NOTIFICATION_CMD, FRIDGE_TEMP_ISSUE_NOTIF, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
				}
				tempWatchdog = 0;
			}
		}
//###- Manage the HeartBeat message
		if(heartBeatCounter > HEARTBEAT_TIMEOUT){
			heartBeatCounter = 0;
			sendResponse(FRIDGE_CMD, HB_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
		}

//###- Manage refrigerator running modes
		if(fridgeMode == OFF_MODE){
			if(fridgeStatus == 0){
				HAL_GPIO_WritePin(COOLING_FAN_GPIO_Port, COOLING_FAN_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(ADD_AC_RELAY_1_EN_GPIO_Port, ADD_AC_RELAY_1_EN_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(COMPRESSOR_EN_GPIO_Port, COMPRESSOR_EN_Pin, GPIO_PIN_RESET);
				RecircFan_TargetDutyCycle = 0;
				if(setCompressorOn == 1){
					setCompressorOn = 0;
					fridgeCompressorOffTimer = 0;
				}
				heaterRun = 0;
				fridgeStatus = 1;
			}
		}
		//###
		//COOLING MODE
		//###
		else if(fridgeMode == COOLING_MODE){
			//Turn off the defrost heater
			if(heaterRun == 1)
				heaterRun = 0;
			//Cooling sequence
			if(fridgeStatus == 0){
				fridgeStatus = 1;
				HAL_GPIO_WritePin(COOLING_FAN_GPIO_Port, COOLING_FAN_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(ADD_AC_RELAY_1_EN_GPIO_Port, ADD_AC_RELAY_1_EN_Pin, GPIO_PIN_SET);
//				RecircFan_TargetDutyCycle = 9900;
			}
			else if((fridgeStatus == 1) && (fridgeTargetTemp < averageFridgeTemp) && (fridgeCompressorOffTimer > COMPRESSOR_OFF_TIMEOUT)){
				setCompressorOn = 1;
//				HAL_GPIO_WritePin(COOLING_FAN_GPIO_Port, COOLING_FAN_Pin, GPIO_PIN_SET);
				RecircFan_TargetDutyCycle = 9000;
				fridgeStatus = 2;
			}
			else if((fridgeStatus == 2) && ((fridgeTargetTemp - fridgeTemperatureDifferential) > averageFridgeTemp)){
				setCompressorOn = 0;
				HAL_GPIO_WritePin(COMPRESSOR_EN_GPIO_Port, COMPRESSOR_EN_Pin, GPIO_PIN_RESET);
//				HAL_GPIO_WritePin(COOLING_FAN_GPIO_Port, COOLING_FAN_Pin, GPIO_PIN_RESET);
				RecircFan_TargetDutyCycle = 9000;
				fridgeCompressorOffTimer = 0;
				fridgeStatus = 1;
			}
			//Defrost timeout check
			if((defrostCycleTimer > DEFROST_TIMEOUT) && (averageFridgeTemp < (fridgeTargetTemp - fridgeTemperatureDifferential + 0.5))){
				fridgeMode = DEFROST_MODE;
				fridgeStatus = 0;
				defrostCycleTimer = 0;
			}
		}
		//###
		//DEFROST MODE
		//###
		else if(fridgeMode == DEFROST_MODE){
			if(fridgeStatus == 0){
				HAL_GPIO_WritePin(COOLING_FAN_GPIO_Port, COOLING_FAN_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(ADD_AC_RELAY_1_EN_GPIO_Port, ADD_AC_RELAY_1_EN_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(COMPRESSOR_EN_GPIO_Port, COMPRESSOR_EN_Pin, GPIO_PIN_RESET);
				RecircFan_TargetDutyCycle = 0;
				if(setCompressorOn == 1){
					setCompressorOn = 0;
					fridgeCompressorOffTimer = 0;
				}
				heaterRun = 1;
				defrostRunningTime = 0;
				tempDefrostTriggered = 0;
				oldEvaporatorTemp = 0.1;
				fridgeStatus = 1;
			}
			else if((fridgeStatus == 1) && ((defrostRunningTime > DEFROST_RUN_TIME) || (tempDefrostTriggered == 1))){
				heaterRun = 0;
				fridgeStatus = 0;
				defrostCycleTimer = 0;
				fridgeMode = COOLING_MODE;
			}
			//Temperature check
			if((sensorValues[EVAPORATOR_TEMP_ID] > 0.1) && (defrostRunningTime % 20000 == 0) && (defrostRunningTime > 20000)){
				if((sensorValues[EVAPORATOR_TEMP_ID] - oldEvaporatorTemp) > 0.7)
					tempDefrostTriggered = 1;
				else
					oldEvaporatorTemp = sensorValues[EVAPORATOR_TEMP_ID];
			}
		}

//###- Manage cooling fan ramp up and ramp down
		if(RecircFan_Timer >= 10){
			RecircFan_Timer = 0;
			if(RecircFan_TargetDutyCycle > RecircFan_DutyCycle){
				RecircFan_DutyCycle += 10;
				RecircFan_DutyCycle_Config(RecircFan_DutyCycle);
			}
			else if(RecircFan_TargetDutyCycle < RecircFan_DutyCycle){
				if(RecircFan_DutyCycle > 100){ RecircFan_DutyCycle -= 100;}
				else{ RecircFan_DutyCycle = 0;}
				RecircFan_DutyCycle_Config(RecircFan_DutyCycle);
			}
		}

		osThreadYield();
	}
}

/*	Continuous analog sensors reading function
 * 	Parameters: constant argument not used
 * 	No Return:
 */
void AdcSenseTask(void const * argument){
	uint8_t hsMode = 0, hsStatus = 0, capSensorID = 0, movingAvgPos = 0;
	double aux = 0;
	/*	f_adc = 21MHz  -->  f_ph1/2
	 * 	t_adc(sample) = 492 * 2 / f_adc = 46.86us
	 */
	while(1) {
		updateAverageValue();
		if(readCurrentSample() == 0){
			for(int i=0; i<ADC_NUMBER_SENSORS; i++)
				AdcMovingAvg[i][movingAvgPos] = readSensor(i);
			movingAvgPos++;
			if(movingAvgPos >= MOVING_AVG_SAMPLES)
				movingAvgPos = 0;
			//
			for(int i=0; i<ADC_NUMBER_SENSORS; i++){
				sensorValues[i] = 0;
				for(int j=0; j<MOVING_AVG_SAMPLES; j++)
					sensorValues[i] += AdcMovingAvg[i][j] / (float)MOVING_AVG_SAMPLES;
			}
			averageFridgeTemp = (sensorValues[RIGHT_WALL_TEMP_ID] + sensorValues[LEFT_WALL_TEMP_ID]) / 2.0;
		}

		// Read humidity sensor
		if((humSensorConfigured == 1) && (humSensorSeqRead == 0)){
			if(hsStatus == 0){
				if((hsMode == 0) && (humiditySensorRequestHumidityRead() == 0)){
					hsMode = 1;
					hsStatus = 1;
					humSensorTimer = 0;
				}
				else if((hsMode == 1) && (humiditySensorRequestTempRead() == 0)){
					hsMode = 0;
					hsStatus = 1;
					humSensorTimer = 0;
				}
			}
			else if((hsStatus == 1) && (humSensorTimer > HDC1080_READ_TIMEOUT)){
				aux = humiditySensorReadSerialValue();
				if(hsMode == 0){
					if(aux == 0xffff) { humSensorTempValue = 0;}
					else{
						aux = (aux/pow(2,16))*165.0 - 40.0;
						humSensorTempValue = aux * 100; //[�C * 100]
					}
				}
				else if(hsMode == 1){
					if(aux == 0xffff) { humSensorRelHumValue = 0;}
					else{
						aux = (aux/pow(2,16))*100.0;
						humSensorRelHumValue = aux *100; //[%HR * 100]
					}
				}
				hsStatus = 0;
			}
		}
		else if((humSensorConfigured == 1) && (humSensorSeqRead == 1)){
			if((hsStatus == 0) && (humiditySensorRequestTempRead() == 0)){
				hsStatus = 1;
				humSensorTimer = 0;
			}
			else if((hsStatus == 1) && (humSensorTimer > HDC1080_READ_TIMEOUT)){
				uint32_t data = humiditySensorReadSequentialValue();
				if(data != 0xffff){
					aux = ((double)(data & 0x00ff))/pow(2,16)*165.0 - 40.0;
					humSensorTempValue = aux * 100;
					//
					aux = ((double)((data & 0xff00) >> 16))/pow(2,16)*100.0;
					humSensorRelHumValue = aux *100;
					//
					hsStatus = 0;
				}
				else{
					humSensorTempValue = 0;
					humSensorRelHumValue = 0;
				}
			}
		}

		// develop code to read frost detector
		if(capSensorConfigured == 1){
			if(capSensorTimer > 100){
				aux = frostSensorReadValue(capSensorID);
				if(aux == 0xffff) {
					capSensorValue[capSensorID] = 0;
					capSensorConfigured = 0;
				}
				else{
					capSensorValue[capSensorID] = aux;
					if(capSensorID == 0){ capSensorID = 1;}
					else{ capSensorID = 0;}
				}
				capSensorTimer = 0;
			}
		}

		osDelay(ADC_TBS);
		osThreadYield();
	}
}

/*	CAN port transmission function, a delay of 8ms is done between sent commands
 * 	Parameters: constant argument not used
 * 	No Return:
 */
void CanTxTask(void const * argument){
	canBufferTxReset();
	uint8_t sent = 0, error = 0;

	while(1){
		if(canTxThreadConfingured == 0)
			canTxThreadConfingured = 1;
		if((canSendAvailable == 1) && (synchronizationTimer >= 1400) && (synchronizationTimer <= 1500)){
			if(canTxBufferItems > 0){
				sent = error = 0;
				while(sent == 0){
					if(sendNextItemFromCanTxBuffer() == 2){ error ++;}
					else{ sent ++;}
					//
					if(error > 10){ ErrorHandler(CAN_ERROR_CMD);}
					else if(error > 0){ osDelay(10);}
				}
			}
			canSendAvailable = 0;
		}
		osThreadYield();
	}
}

/*	CAN port reception function
 * 	Parameters: constant argument not used
 * 	No Return:
 */
void CanRxTask(void const * argument){
	canBufferRxReset();

	while(1) {
		if(canReceivedMessage == 1){
			while(addItem2CanRxBuffer() != 0){}
			canReceivedMessage = 0;
		}
		else if(canRxBufferItems > 0){
			if(osMutexWait(canRxMutexHandle, 100) == osOK){
				if(canRxBuffer[0][1] == FRIDGE_CMD){
					HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
					//#######
					//##################### Updating Firmware process
					if(canRxBuffer[0][2] == FW_UPDATE_CMD){
						if(OTArunning == 0){
							OTAtimeCounter = 0;
							OTArunning = 1;
							osThreadResume(otaHandle);
						}
						else{
							for(int i=0; i<CAN_MSG_LENGTH; i++)
								otaReceivedMessage[i] = canRxBuffer[0][i];
							OTAnewData = 1;
						}
					}
					//#######
					//##################### Replace new firmware version with the one stored in the other memory bank
					else if(canRxBuffer[0][2] == FW_ROLLBACK_CMD){
						if(((*(__IO uint32_t *)(BANK1_ADDRESS)) == DATA_1) && ((*(__IO uint32_t *)(BANK2_ADDRESS)) == DATA_1)){
							ChangeBootingBank();
							sendResponse(canRxBuffer[0][1], FW_ROLLBACK_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
							safeRestart = 1;
						}
						else
							sendResponse(canRxBuffer[0][1], FW_ROLLBACK_CMD, ERROR_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					//#######
					//##################### Read firmware version
					else if(canRxBuffer[0][2] == FW_VERSION_CMD){
						sendResponse(canRxBuffer[0][1], FW_VERSION_CMD, FW_VERSION, FW_SUBVERSION, SUCCESS_CMD, ZERO_CMD, END_CMD);
					}
					//#######
					//##################### Read memory bank in use
					else if(canRxBuffer[0][2] == FW_BANK_CMD){
						sendResponse(canRxBuffer[0][1], FW_BANK_CMD, flashBank, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					//#######
					//##################### Restart microcontroller by software
					else if(canRxBuffer[0][2] == FW_RESTART_CMD){
						sendResponse(canRxBuffer[0][1], FW_RESTART_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						safeRestart = 1;
					}
					//#######
					//##################### Read analog sensor
					else if((canRxBuffer[0][2] >= ADC_OFFSET) && (canRxBuffer[0][2] < ADC_OFFSET + ADC_NUMBER_SENSORS)){
						uint8_t sensorID = canRxBuffer[0][2] - ADC_OFFSET;
						uint8_t sign = 0;
						uint32_t val = (uint32_t)(sensorValues[sensorID]*100.0);
						if(sensorValues[sensorID] < 0){
							sign = 1;
							val = (uint32_t)(sensorValues[sensorID]*(-100.0));
						}
						sendResponse(canRxBuffer[0][1], sensorID+ADC_OFFSET, val, val >> 8, sign, SUCCESS_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == READ_AVG_TEMP_CMD){
						uint8_t sign = 0;
						uint32_t val = (uint32_t)(averageFridgeTemp * 100.0);
						if(averageFridgeTemp < 0){
							sign = 1;
							val = (uint32_t)(averageFridgeTemp * (-100.0));
						}
						sendResponse(canRxBuffer[0][1], READ_AVG_TEMP_CMD, val, val >> 8, sign, SUCCESS_CMD, END_CMD);
					}
					//#######
					//##################### Digital controlled systems
					else if(canRxBuffer[0][2] == DOOR_HEATER_CMD){
						if((canRxBuffer[0][3] != 0) && (canRxBuffer[0][3] != 1))
							sendResponse(canRxBuffer[0][1], DOOR_HEATER_CMD, canRxBuffer[0][3], ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else{
							HAL_GPIO_WritePin(DOOR_HEATER_EN_GPIO_Port, DOOR_HEATER_EN_Pin, canRxBuffer[0][3]);
							sendResponse(canRxBuffer[0][1], DOOR_HEATER_CMD, canRxBuffer[0][3], SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
					}
					else if(canRxBuffer[0][2] == RECIRCULATION_FAN_CMD){
						if((canRxBuffer[0][3] != 0) && (canRxBuffer[0][3] != 1))
							sendResponse(canRxBuffer[0][1], RECIRCULATION_FAN_CMD, canRxBuffer[0][3], ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else{
							if(canRxBuffer[0][3] == 1){ RecircFan_TargetDutyCycle = 9900;}
							else{ RecircFan_TargetDutyCycle = 0;}
							sendResponse(canRxBuffer[0][1], RECIRCULATION_FAN_CMD, canRxBuffer[0][3], SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
					}
					else if(canRxBuffer[0][2] == COOLING_FAN_CMD){
						if((canRxBuffer[0][3] != 0) && (canRxBuffer[0][3] != 1))
							sendResponse(canRxBuffer[0][1], COOLING_FAN_CMD, canRxBuffer[0][3], ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else{
							HAL_GPIO_WritePin(COOLING_FAN_GPIO_Port, COOLING_FAN_Pin, canRxBuffer[0][3]);
							sendResponse(canRxBuffer[0][1], COOLING_FAN_CMD, canRxBuffer[0][3], SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
					}
					else if(canRxBuffer[0][2] == LIGHTS_EN_CMD){
						if((canRxBuffer[0][3] != 0) && (canRxBuffer[0][3] != 1))
							sendResponse(canRxBuffer[0][1], LIGHTS_EN_CMD, canRxBuffer[0][3], ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else{
							HAL_GPIO_WritePin(LIGHTS_EN_GPIO_Port, LIGHTS_EN_Pin, canRxBuffer[0][3]);
							sendResponse(canRxBuffer[0][1], LIGHTS_EN_CMD, canRxBuffer[0][3], SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
					}
					else if(canRxBuffer[0][2] == COMPRESSOR_EN_CMD){
						if((canRxBuffer[0][3] != 0) && (canRxBuffer[0][3] != 1))
							sendResponse(canRxBuffer[0][1], COMPRESSOR_EN_CMD, canRxBuffer[0][3], ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else{
							HAL_GPIO_WritePin(COMPRESSOR_EN_GPIO_Port, COMPRESSOR_EN_Pin, canRxBuffer[0][3]);
							sendResponse(canRxBuffer[0][1], COMPRESSOR_EN_CMD, canRxBuffer[0][3], SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
					}
					else if(canRxBuffer[0][2] == HEATER_EN_CMD){
						if((canRxBuffer[0][3] != 0) && (canRxBuffer[0][3] != 1))
							sendResponse(canRxBuffer[0][1], HEATER_EN_CMD, canRxBuffer[0][3], ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else{
							HAL_GPIO_WritePin(HEATER_EN_GPIO_Port, HEATER_EN_Pin, canRxBuffer[0][3]);
							sendResponse(canRxBuffer[0][1], HEATER_EN_CMD, canRxBuffer[0][3], SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
					}
					else if(canRxBuffer[0][2] == ADD_AC_RELAY_1_EN_CMD){
						if((canRxBuffer[0][3] != 0) && (canRxBuffer[0][3] != 1))
							sendResponse(canRxBuffer[0][1], ADD_AC_RELAY_1_EN_CMD, canRxBuffer[0][3], ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else{
							HAL_GPIO_WritePin(ADD_AC_RELAY_1_EN_GPIO_Port, ADD_AC_RELAY_1_EN_Pin, canRxBuffer[0][3]);
							sendResponse(canRxBuffer[0][1], ADD_AC_RELAY_1_EN_CMD, canRxBuffer[0][3], SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
					}
					else if(canRxBuffer[0][2] == ADD_AC_RELAY_2_EN_CMD){
						if((canRxBuffer[0][3] != 0) && (canRxBuffer[0][3] != 1))
							sendResponse(canRxBuffer[0][1], ADD_AC_RELAY_2_EN_CMD, canRxBuffer[0][3], ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else{
							HAL_GPIO_WritePin(ADD_AC_RELAY_2_EN_GPIO_Port, ADD_AC_RELAY_2_EN_Pin, canRxBuffer[0][3]);
							sendResponse(canRxBuffer[0][1], ADD_AC_RELAY_2_EN_CMD, canRxBuffer[0][3], SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
					}
					else if(canRxBuffer[0][2] == GET_DEV_STATUS_CMD){
						sendResponse(canRxBuffer[0][1], GET_DEV_STATUS_CMD, RecircFan_DutyCycle/100, setCompressorOn, heaterRun, SUCCESS_CMD, END_CMD);
					}

					//#######
					//##################### Running sequences
					else if(canRxBuffer[0][2] == START_COOLING_SEQ_CMD){
						fridgeMode = COOLING_MODE;
						fridgeStatus = 0;
						sendResponse(canRxBuffer[0][1], START_COOLING_SEQ_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == START_DEFROST_SEQ_CMD){
						fridgeMode = DEFROST_MODE;
						fridgeStatus = 0;
						sendResponse(canRxBuffer[0][1], START_DEFROST_SEQ_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == STOP_SEQ_CMD){
						fridgeMode = OFF_MODE;
						fridgeStatus = 0;
						sendResponse(canRxBuffer[0][1], STOP_SEQ_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == SET_TARGET_TEMP_CMD){
						float aux = ((float)(canRxBuffer[0][3] | (canRxBuffer[0][4] << 8))) / 100.0;
						if(aux < (fridgeTemperatureDifferential+0.1)){ fridgeTargetTemp = fridgeTemperatureDifferential + 0.1;}
						fridgeTargetTemp = aux;
						sendResponse(canRxBuffer[0][1], SET_TARGET_TEMP_CMD, (uint32_t)(fridgeTargetTemp*100.0), ((uint32_t)(fridgeTargetTemp*100.0)) >> 8, SUCCESS_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == GET_TARGET_TEMP_CMD){
						sendResponse(canRxBuffer[0][1], GET_TARGET_TEMP_CMD, (uint32_t)(fridgeTargetTemp*100.0), ((uint32_t)(fridgeTargetTemp*100.0)) >> 8, SUCCESS_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == LOCK_BUTTON_PANEL_CMD){
						fridgeLockStatus = LOCK_FRIDGE;
						sendResponse(canRxBuffer[0][1], LOCK_BUTTON_PANEL_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == UNLOCK_BUTTON_PANEL_CMD){
						fridgeLockStatus = UNLOCK_FRIDGE;
						sendResponse(canRxBuffer[0][1], UNLOCK_BUTTON_PANEL_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == GET_RUN_MODE_CMD)
						sendResponse(canRxBuffer[0][1], GET_RUN_MODE_CMD, fridgeMode, fridgeStatus, SUCCESS_CMD, ZERO_CMD, END_CMD);
					else if(canRxBuffer[0][2] == GET_DEFROST_TIMER_VAL_CMD){
						uint32_t aux = defrostCycleTimer/1000;
						sendResponse(canRxBuffer[0][1], GET_DEFROST_TIMER_VAL_CMD, aux, aux >> 8, SUCCESS_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == SET_DEFROST_POWER_MODE_CMD){
						if((canRxBuffer[0][3] >= 0) && (canRxBuffer[0][3] <= 10)){
							DH_TargetPowerMode = canRxBuffer[0][3];
							sendResponse(canRxBuffer[0][1], SET_DEFROST_POWER_MODE_CMD, DH_TargetPowerMode, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
						else
							sendResponse(canRxBuffer[0][1], SET_DEFROST_POWER_MODE_CMD, DH_TargetPowerMode, ERROR_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					//#######
					//##################### Humidity sensor
					else if(canRxBuffer[0][2] == GET_HUMIDITY_CMD){
						sendResponse(canRxBuffer[0][1], GET_HUMIDITY_CMD, humSensorRelHumValue, humSensorRelHumValue >> 8, SUCCESS_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == GET_HUMIDITY_TEMP_CMD){
						sendResponse(canRxBuffer[0][1], GET_HUMIDITY_TEMP_CMD, humSensorTempValue, humSensorTempValue >> 8, SUCCESS_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == HUMIDITY_HEATER_ON_CMD){
						if(humiditySensorEnableHeater() == 0)
							sendResponse(canRxBuffer[0][1], HUMIDITY_HEATER_ON_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else
							sendResponse(canRxBuffer[0][1], HUMIDITY_HEATER_ON_CMD, ERROR_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == HUMIDITY_HEATER_OFF_CMD){
						if(humiditySensorDisableHeater() == 0)
							sendResponse(canRxBuffer[0][1], HUMIDITY_HEATER_OFF_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						else
							sendResponse(canRxBuffer[0][1], HUMIDITY_HEATER_OFF_CMD, ERROR_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == HUMIDITY_SERIAL_READ_CMD){
						if(humiditySensorSerialReadConfig() == 0){
							humSensorSeqRead = 0;
							sendResponse(canRxBuffer[0][1], HUMIDITY_SERIAL_READ_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
						else
							sendResponse(canRxBuffer[0][1], HUMIDITY_SERIAL_READ_CMD, ERROR_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					else if(canRxBuffer[0][2] == HUMIDITY_SEQ_READ_CMD){
						if(humiditySensorSequentialReadConfig() == 0){
							humSensorSeqRead = 1;
							sendResponse(canRxBuffer[0][1], HUMIDITY_SEQ_READ_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
						}
						else
							sendResponse(canRxBuffer[0][1], HUMIDITY_SEQ_READ_CMD, ERROR_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
					}
					//#######
					//##################### Frost detection sensor
					else if(canRxBuffer[0][2] == GET_FROST_1_CAP_CMD){
						if(capSensorConfigured == 0){ sendResponse(canRxBuffer[0][1], GET_FROST_1_CAP_CMD, capSensorValue[0], capSensorValue[0]>>8, ERROR_CMD, ZERO_CMD, END_CMD);}
						else { sendResponse(canRxBuffer[0][1], GET_FROST_1_CAP_CMD, capSensorValue[0], capSensorValue[0]>>8, SUCCESS_CMD, ZERO_CMD, END_CMD);}
					}
					else if(canRxBuffer[0][2] == GET_FROST_2_CAP_CMD){
						if(capSensorConfigured == 0){ sendResponse(canRxBuffer[0][1], GET_FROST_2_CAP_CMD, capSensorValue[1], capSensorValue[1]>>8, ERROR_CMD, ZERO_CMD, END_CMD);}
						else { sendResponse(canRxBuffer[0][1], GET_FROST_2_CAP_CMD, capSensorValue[1], capSensorValue[1]>>8, SUCCESS_CMD, ZERO_CMD, END_CMD);}
					}
				}
				readNextItemFromCanRxBuffer();
				osMutexRelease(canRxMutexHandle);
			}
		}
		osThreadYield();
	}
}

/*	Defrost heater control
 * 	Parameters: constant argument not used
 * 	No Return:
 */
void HeaterTask(void const * argument){
	while(1){
		//Heater power mode selection
		if((heaterRun == 1) && (DH_ZCcounter == 0)){
			DH_PowerMode = DH_TargetPowerMode;
			HAL_GPIO_WritePin(DOOR_HEATER_EN_GPIO_Port, DOOR_HEATER_EN_Pin, GPIO_PIN_SET);
		}
		else if((heaterRun == 0) && (DH_ZCcounter == 0)){
			DH_PowerMode = 0;
			HAL_GPIO_WritePin(DOOR_HEATER_EN_GPIO_Port, DOOR_HEATER_EN_Pin, GPIO_PIN_RESET);
		}

		//Check compressor
		if(setCompressorOn == 0)
			HAL_GPIO_WritePin(COMPRESSOR_EN_GPIO_Port, COMPRESSOR_EN_Pin, GPIO_PIN_RESET);
		else
			HAL_GPIO_WritePin(COMPRESSOR_EN_GPIO_Port, COMPRESSOR_EN_Pin, GPIO_PIN_SET);

		//Check power mode heater
		osDelay(5);
		if((DH_POWER_MATRIX[DH_PowerMode][(uint8_t)DH_ZCcounter] == 0))
			HAL_GPIO_WritePin(HEATER_EN_GPIO_Port, HEATER_EN_Pin, GPIO_PIN_RESET);
		else if(DH_POWER_MATRIX[DH_PowerMode][(uint8_t)DH_ZCcounter] == 1)
			HAL_GPIO_WritePin(HEATER_EN_GPIO_Port, HEATER_EN_Pin, GPIO_PIN_SET);
		//
//		HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
		osThreadSuspend(NULL);
	}
}

/*	Update the firmware with the file received via CAN bus
 * 	Parameters: constant argument not used
 * 	No Return:
 */
void FirmwareUpdateTask(void const * argument){
	uint8_t status = 0, error = 0;
	uint32_t fileSize = 0, writingAddress = 0, applicationAddress = 0, writtenWords = 0;

	while(1){
		if((OTArunning == 1) && (OTAtimeCounter < FW_UPDATE_TIMEOUT)){
			//Erase flash bank where we want to store the new firmware
			if(status == 0){
				//initialize checksum
				for(int id=0; id < WORD_BYTES; id++){ checksum[id] = 0x00;}
				//Initialize writing address & erase bank
				if(flashBank == 1){
					writingAddress = applicationAddress = BANK2_ADDRESS;
					if(FLASH_If_EraseBank(FLASH_BANK_2) != 0){ ErrorHandler(FL_ERROR_ERASING);}
					else{status = 1;}
				}
				else if(flashBank == 2){
					writingAddress = applicationAddress = BANK1_ADDRESS;
					if(FLASH_If_EraseBank(FLASH_BANK_1) != 0){ ErrorHandler(FL_ERROR_ERASING);}
					else{status = 1;}
				}
				else{ ErrorHandler(FL_ERROR_NO_CORRECT_BANK);}
				//
				HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
				sendResponse(FRIDGE_CMD, FW_UPDATE_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
			}
			else if(OTAnewData == 1){
				HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
				//Read file size
				if(status == 1){
					for(uint8_t i=0; i<WORD_BYTES; i++){
						fileSize = fileSize | (otaReceivedMessage[i+3] << (i*8));
					}
					HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
					status = 2;
					writingAddress = applicationAddress = BANK2_ADDRESS;
					sendResponse(FRIDGE_CMD, FW_UPDATE_CMD, otaReceivedMessage[3], otaReceivedMessage[4], otaReceivedMessage[5], otaReceivedMessage[6], SUCCESS_CMD);
				}
				//Store in memory the read data
				else if(status == 2){
					uint32_t data = 0;
					for(uint8_t i=0; i<WORD_BYTES; i++){
						data = data | (otaReceivedMessage[i+3] << (i*8));
					}
					//Check information before store in memory
					if((writtenWords == 0) && (data != DATA_1)){
						ErrorHandler(FL_ERROR_BINARY_FILE);
					}
					else if((data != DATA_2)&&(((writtenWords >= 7)&&(writtenWords < 11))||(writtenWords==13)||(writtenWords==95))){
						ErrorHandler(FL_ERROR_BINARY_FILE);
					}
					else if(((data & 0xfff00000) != DATA_3)&&(((writtenWords >= 1)&&(writtenWords < 7))||((writtenWords >= 14)&&(writtenWords < 95))||(writtenWords==11)||(writtenWords==12))){
						ErrorHandler(FL_ERROR_BINARY_FILE);
					}
					//Store the receive data in memory with error managing
					error = 1;
					FLASH_If_Init();
					//Store the receive data in memory with error managing
					while(error != 0){
						if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, writingAddress, data) == HAL_OK){
							HAL_FLASH_Lock();
							//Verification
							if(data != *(__IO uint32_t *)(writingAddress)){
								if(error > 5){
									sendResponse(FRIDGE_CMD, FW_UPDATE_CMD, otaReceivedMessage[3], otaReceivedMessage[4], otaReceivedMessage[5], otaReceivedMessage[6], ERROR_CMD);
									status = 5;
									osDelay(1000);
									break;
								}
								else{error ++;}
							}
							else{
								for(uint8_t i=0; i<WORD_BYTES; i++){
									checksum[i] += *(__IO uint8_t *)(writingAddress);
									writingAddress ++;
								}
								writtenWords ++;
								HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
								error = 0;
								sendResponse(FRIDGE_CMD, FW_UPDATE_CMD, checksum[0], checksum[1], checksum[2], checksum[3], SUCCESS_CMD);
//								sendResponse(FRIDGE_CMD, FW_UPDATE_CMD, otaReceivedMessage[3], otaReceivedMessage[4], otaReceivedMessage[5], otaReceivedMessage[6], SUCCESS_CMD);
							}
						}
						else{
							if(error > 5){ ErrorHandler(FL_ERROR_WRITING);}
							else{ error ++;}
						}
					}
					//Check the quantity of written data
					if(writingAddress >= applicationAddress + fileSize){ status = 3;}
				}
				//Read checksum and compare with checksum of stored data in memory
				else if(status == 3){
					error = 0;
					writingAddress = applicationAddress;
					for(int id=0; id < WORD_BYTES; id++){ checksum[id] = 0x00;}
					//Obtain checksum
					while(writingAddress < (applicationAddress + fileSize)){
						for(uint8_t i=0; i<WORD_BYTES; i++){
							checksum[i] += *(__IO uint8_t *)(writingAddress);
							writingAddress ++;
						}
					}
					//Compare checksum
					for(uint8_t i=0; i<WORD_BYTES; i++){
						if(checksum[i] != otaReceivedMessage[i+3]){ error ++;}
					}
					//
					if(error != 0){
						status = 5;
						sendResponse(FRIDGE_CMD, FW_UPDATE_CMD, checksum[0], checksum[1], checksum[2], checksum[3], ERROR_CMD);
					}
					else{
						status = 4;
						sendResponse(FRIDGE_CMD, FW_UPDATE_CMD, checksum[0], checksum[1], checksum[2], checksum[3], SUCCESS_CMD);
					}
					osDelay(1000);
				}
				//Reset memory
				OTAnewData = 0;
			}
			//Send message with the status of the operation
			else if((status == 4) || (status == 5)){
				//Disable interferences
				osDelay(1000);
				HAL_NVIC_DisableIRQ(DEFROST_BTN_IRQn);
				HAL_NVIC_DisableIRQ(DECREASE_SET_TEMP_BTN_IRQn);
				HAL_NVIC_DisableIRQ(INCREASE_SET_TEMP_BTN_IRQn);
				HAL_NVIC_DisableIRQ(CANx_RX0_IRQn);
				HAL_NVIC_DisableIRQ(I2Cx_EV_IRQn);
				HAL_NVIC_DisableIRQ(I2Cx_ER_IRQn);
				HAL_NVIC_DisableIRQ(AC_ZC_DETECT_IRQn);
				HAL_NVIC_DisableIRQ(US_TIMER_IRQn);
				HAL_NVIC_DisableIRQ(TIM8_UP_TIM13_IRQn);
				HAL_NVIC_DisableIRQ(ADC_DMA_STREAM_IRQn);
				//Restart through the other bank if it is not empty
				if(status == 4){ ChangeBootingBank();}
				else{
					if(flashBank == 1){
						if(FLASH_If_EraseBank(FLASH_BANK_2) != 0){ ErrorHandler(FL_ERROR_ERASING);}
					}
					else if(flashBank == 2){
						if(FLASH_If_EraseBank(FLASH_BANK_1) != 0){ ErrorHandler(FL_ERROR_ERASING);}
					}
					HAL_FLASH_Lock();
				}
				HAL_NVIC_SystemReset();
			}
		}
		else if((OTArunning == 1) && (OTAtimeCounter > FW_UPDATE_TIMEOUT)){
			ErrorHandler(FL_ERROR_TIMEOUT);
		}
		else if(OTArunning == 0){
			osThreadSuspend(NULL);
		}
		osThreadYield();
	}
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM13 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if (htim->Instance == TIM13) {
		HAL_IncTick();
		initialDelayTimer ++;
		HalfSecondTimer ++;
		heartBeatCounter ++;
		fridgeCompressorOffTimer ++;
		defrostRunningTime ++;
		defrostCycleTimer ++;
		decreaseTempButtonTimer ++;
		humSensorTimer ++;
		capSensorTimer ++;
		defrostButtonTimer ++;
		increaseTempButtonTimer ++;
		RecircFan_Timer ++;
		tempWatchdog ++;
	}
	else if(US_TIMER){
		synchronizationTimer ++;
	}
}

/*	Callback that runs every interruption detected in the zero cross detection pin
 * 	Parameters: Pin ID where the interruption was detected
 * 	No Return:
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	//##- Zero cross detector: HEATER
	if((GPIO_Pin == AC_ZC_DETECT_Pin) && (StartInterruption == 1)){
		if(heaterZC == 1){
			if(DH_ZCcounter < (DH_CTRL_CYCLES-1))
			  DH_ZCcounter ++;
			else
			  DH_ZCcounter = 0;
			//
			heaterZC = 0;
			osThreadResume(heaterHandle);
//			HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
		}
		else{
			heaterZC = 1;
		}
	}

	//##- Decrease temperature button
	else if((GPIO_Pin == DECREASE_SET_TEMP_BTN_Pin) && (HAL_GPIO_ReadPin(DECREASE_SET_TEMP_BTN_GPIO_Port, DECREASE_SET_TEMP_BTN_Pin) == GPIO_PIN_SET)){
		decreaseTempButtonTimer = 0;
	}
	else if((GPIO_Pin == DECREASE_SET_TEMP_BTN_Pin) && (HAL_GPIO_ReadPin(DECREASE_SET_TEMP_BTN_GPIO_Port, DECREASE_SET_TEMP_BTN_Pin) == GPIO_PIN_RESET) &&
			(fridgeLockStatus != LOCK_FRIDGE)){
		//short press
		if((decreaseTempButtonTimer < 3000) && (decreaseTempButtonTimer > 100)){
			if((fridgeTargetTemp - 0.5 >= (fridgeTemperatureDifferential + 0.1)) && (fridgeTargetTemp > fridgeTemperatureDifferential))
				fridgeTargetTemp -= 0.5;
			else
				fridgeTargetTemp = fridgeTemperatureDifferential + 0.1;
		}
		//long press
		else if(decreaseTempButtonTimer >= 3000){
			fridgeMode = OFF_MODE;
			fridgeStatus = 0;
		}
	}

	//##- Increase temperature button
	else if((GPIO_Pin == INCREASE_SET_TEMP_BTN_Pin) && (HAL_GPIO_ReadPin(INCREASE_SET_TEMP_BTN_GPIO_Port, INCREASE_SET_TEMP_BTN_Pin) == GPIO_PIN_SET)){
		increaseTempButtonTimer = 0;
	}
	else if((GPIO_Pin == INCREASE_SET_TEMP_BTN_Pin) && (HAL_GPIO_ReadPin(INCREASE_SET_TEMP_BTN_GPIO_Port, INCREASE_SET_TEMP_BTN_Pin) == GPIO_PIN_RESET) &&
			(fridgeLockStatus != LOCK_FRIDGE)){
		//short press
		if((increaseTempButtonTimer < 3000) && (increaseTempButtonTimer > 100)){
			if(fridgeTargetTemp + 0.5 <= 10)
				fridgeTargetTemp += 0.5;
		}
		//long press
		else if(increaseTempButtonTimer >= 3000){
			fridgeMode = COOLING_MODE;
			fridgeStatus = 0;
		}
	}

	//##- Defrost button
	else if((GPIO_Pin == DEFROST_BTN_Pin) && (HAL_GPIO_ReadPin(DEFROST_BTN_GPIO_Port, DEFROST_BTN_Pin) == GPIO_PIN_SET)){
		defrostButtonTimer = 0;
	}
	else if((GPIO_Pin == DEFROST_BTN_Pin) && (HAL_GPIO_ReadPin(DEFROST_BTN_GPIO_Port, DEFROST_BTN_Pin) == GPIO_PIN_RESET) && (fridgeLockStatus != LOCK_FRIDGE)){
		//short press
		if((defrostButtonTimer < 3000) && (defrostButtonTimer > 100))
			HAL_GPIO_TogglePin(LIGHTS_EN_GPIO_Port, LIGHTS_EN_Pin);
		//long press
		else if(defrostButtonTimer >= 3000){
			fridgeMode = DEFROST_MODE;
			fridgeStatus = 0;
		}
	}
}

/*  RX FIFO pending message callback
 *  Parameters:  pointer to a can handler that contains all the specific information of the CAN port
 *  No Return
 */
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan){
	if(CAN_ReceiveData() == 0){
		if(CAN_GetIdFromReceivedMessage() == ACD_CAN_ID){
			//Check that the initial and final bytes are correct
			if((CAN_GetReceivedMessage(0) == START_CMD) && (CAN_GetReceivedMessage(7) == END_CMD)){
				if(CAN_GetReceivedMessage(1) == SYNCH_CMD){
					synchronizationTimer = 0;
					canSendAvailable = 1;
				}
				else{
					canReceivedMessage = 1;
				}
			}
		}
	}
}

/*	Send message via CAN Bus
 * 	Parameters: 1 byte for command and 4 bytes of data to send, mainly for answer with a successful status or and error status
 * 	No Return:
 */
void sendResponse(uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7){
	uint8_t sendData[CAN_MSG_LENGTH];
	uint8_t add = 0;
	sendData[0] = START_CMD;
	sendData[1] = d1;
	sendData[2]	= d2;
	sendData[3]	= d3;
	sendData[4] = d4;
	sendData[5] = d5;
	sendData[6] = d6;
	sendData[7] = d7;
	//Wait to get the mutex before to access to the CAN memory data
	while(add == 0){
		if(addItem2CanTxBuffer(sendData) >= 0){ add = 1;}
		osDelay(1);
	}
}

/*	Add item to the CAN Tx communication buffer
 * 	Parameters: byte array to add to the matrix buffer
 * 	Return:
 * 		0: no error
 * 		1: error
 */
static uint8_t addItem2CanTxBuffer(uint8_t newItem[CAN_MSG_LENGTH]){
	if(canTxBufferItems == CAN_BUFFER_LENGTH){
		canBufferTxReset();
		uint8_t bufferOverflow[CAN_MSG_LENGTH] = { START_CMD, PCB_NOTIFICATION_CMD, CAN_TX_BUFFER_OVERFLOW_NOTIF, FRIDGE_CMD, SUCCESS_CMD, ZERO_CMD, ZERO_CMD, END_CMD};
		while(osMutexWait(canTxMutexHandle, 100) != osOK){}
		for(uint8_t i=0; i<CAN_MSG_LENGTH; i++){ canTxBuffer[canTxBufferItems][i] = bufferOverflow[i];}
		canTxBufferItems ++;
		osMutexRelease(canTxMutexHandle);
		return 1;
	}
	else{
		while(osMutexWait(canTxMutexHandle, 100) != osOK){}
		for(uint8_t i=0; i<CAN_MSG_LENGTH; i++){ canTxBuffer[canTxBufferItems][i] = newItem[i];}
		canTxBufferItems ++;
		osMutexRelease(canTxMutexHandle);
		return 0;
	}
}

/*	Send item to the CAN Tx communication buffer, move the rest of items one position
 * 	Parameters:
 * 	Return:
 * 		0: no error
 * 		1: error buffer is empty
 * 		2: error message is not sent
 */
static uint8_t sendNextItemFromCanTxBuffer(void){
	if(canTxBufferItems == 0){ return 1;}
	else{
		while(osMutexWait(canTxMutexHandle, 100) != osOK){}
		if(CAN_SendData(canTxBuffer[0], CAN_MSG_LENGTH) == 2){
			osMutexRelease(canTxMutexHandle);
			return 2;
		}
		else{
			//reorder the list of messages to sent
			for(int i=0; i<canTxBufferItems; i++){
				for(uint8_t j=0; j<CAN_MSG_LENGTH; j++){
					if(i == (CAN_BUFFER_LENGTH - 1)){ canTxBuffer[i][j] = 0;}
					else{ canTxBuffer[i][j] = canTxBuffer[i+1][j];}
				}
			}
			canTxBufferItems --;
			osMutexRelease(canTxMutexHandle);
		}
		return 0;
	}
}

/*	Clear all the CAN Tx buffer information
 * 	No Parameters:
 * 	No Return:
 */
static void canBufferTxReset(void){
	while(osMutexWait(canTxMutexHandle, 100) != osOK){}
	canTxBufferItems = 0;
	for(uint8_t i=0; i<CAN_BUFFER_LENGTH; i++){
		for(uint8_t j=0; j<CAN_MSG_LENGTH; j++){ canTxBuffer[i][j] = 0;}
	}
	osMutexRelease(canTxMutexHandle);
}

/*	Add item to the CAN Rx communication buffer
 * 	Parameters: byte array to add to the matrix buffer
 * 	Return:
 * 		0: no error
 * 		1: error
 */
static uint8_t addItem2CanRxBuffer(void){
	if(canRxBufferItems == CAN_BUFFER_LENGTH){ return 1;}
	else{
		while(osMutexWait(canRxMutexHandle, 100) != osOK){}
		for(uint8_t i=0; i<CAN_MSG_LENGTH; i++){
			canRxBuffer[canRxBufferItems][i] = CAN_GetReceivedMessage(i);
		}
		canRxBufferItems ++;
		osMutexRelease(canRxMutexHandle);
		return 0;
	}
}

/*	Read item to the CAN Rx communication buffer, move the rest of items one position
 * 	Parameters:
 * 	Return:
 * 		0: no error
 * 		1: error buffer is empty
 */
static uint8_t readNextItemFromCanRxBuffer(void){
	if(canRxBufferItems == 0){ return 1;}
	else{
		for(int i=0; i<canRxBufferItems; i++){
			for(uint8_t j=0; j<CAN_MSG_LENGTH; j++){
				if(i == (CAN_BUFFER_LENGTH - 1)){ canRxBuffer[i][j] = 0;}
				else{ canRxBuffer[i][j] = canRxBuffer[i+1][j];}
			}
		}
		canRxBufferItems --;
		return 0;
	}
}

/*	Clear all the CAN Rx buffer information
 * 	No Parameters:
 * 	No Return:
 */
static void canBufferRxReset(void){
	while(osMutexWait(canRxMutexHandle, 100) != osOK){}
	canRxBufferItems = 0;
	for(uint8_t i=0; i<CAN_BUFFER_LENGTH; i++){
		for(uint8_t j=0; j<CAN_MSG_LENGTH; j++){ canRxBuffer[i][j] = 0;}
	}
	osMutexRelease(canRxMutexHandle);
}

/*  PWM timer channels configuration for controlling the Recirc fan
 * 	No Parameters:
 * 	Return:
 * 		0: no error
 *		1: configuration error
 */
static uint8_t RecircFan_PWM_Config(void){
	//set timer to generate a signal frequency of 10kHz
	RecircFan_Period = (SystemCoreClock / 20000 ) - 1;
	RecircFan_Pulse = (0 * (RecircFan_Period - 1)) / 100;	// 0%
	//#####
	//###############  LEFT GROUP
	RecircFan_TimerHandle.Instance 				= RECIRC_FAN_TIM;
	RecircFan_TimerHandle.Init.Period 			= RecircFan_Period;
	RecircFan_TimerHandle.Init.Prescaler 		= 0;
	RecircFan_TimerHandle.Init.CounterMode 		= TIM_COUNTERMODE_UP;
	RecircFan_TimerHandle.Init.ClockDivision 	= TIM_CLOCKDIVISION_DIV1;
	if (HAL_TIM_PWM_Init(&RecircFan_TimerHandle) != HAL_OK) { return 1;}
	//
	RecircFan_TimerConfig.OCMode 				= TIM_OCMODE_PWM2;
	RecircFan_TimerConfig.OCFastMode 			= TIM_OCFAST_DISABLE;
	RecircFan_TimerConfig.OCPolarity  			= TIM_OCPOLARITY_LOW;
	RecircFan_TimerConfig.OCNPolarity 			= TIM_OCNPOLARITY_HIGH;
	RecircFan_TimerConfig.OCIdleState 			= TIM_OCIDLESTATE_SET;
	RecircFan_TimerConfig.OCNIdleState			= TIM_OCNIDLESTATE_RESET;
	RecircFan_TimerConfig.Pulse 				= RecircFan_Pulse;
	if (HAL_TIM_PWM_ConfigChannel(&RecircFan_TimerHandle, &RecircFan_TimerConfig, RECIRC_FAN_TIM_CHANNEL) != HAL_OK){ return 1;}
	//
	if(HAL_TIM_PWM_Start(&RecircFan_TimerHandle, RECIRC_FAN_TIM_CHANNEL) != HAL_OK){ return 1;}
	HAL_TIM_MspPostInit(&RecircFan_TimerHandle);
	//
	return 0;
}

/*	Modify the duty cycle of the recirculation fan
 * 	Parameters:
 * 		dc: new duty cycle to implement
 * 	Return:
 * 		0: no error
 *		1: configuration error
 */
static uint8_t RecircFan_DutyCycle_Config (uint32_t dc){
	float aux = dc/100.0;
	uint32_t pulse = (aux * (RecircFan_Period - 1)) / 100;
	//
	RecircFan_TimerConfig.Pulse = pulse;
	if (HAL_TIM_PWM_ConfigChannel(&RecircFan_TimerHandle, &RecircFan_TimerConfig, RECIRC_FAN_TIM_CHANNEL) != HAL_OK){ return 1;}
	if(HAL_TIM_PWM_Start(&RecircFan_TimerHandle, RECIRC_FAN_TIM_CHANNEL) != HAL_OK){ return 1;}
	//
	return 0;
}

/*	Access to option bytes to read the status of the dual boot byte
 * 	No Parameters:
 * 	No Return:
 */
static void ReadCurrentBank(void){
	//Allow access to flash control registers and option bytes sector
	HAL_FLASH_Unlock();
	HAL_FLASH_OB_Unlock();
	//Check flash memory is not busy
	while((FLASH->SR & FLASH_SR_BSY) ==  FLASH_SR_BSY);
	//Enable to work with dual bank (bit 30)
	FLASH->OPTCR1 |= FLASH_OPTCR_DB1M;
	FLASH->OPTCR |= FLASH_OPTCR_DB1M;
	//Program BOR level 2 (bits 3 and 2)
	FLASH->OPTCR1 &= ~(1 << (FLASH_OPTCR_BOR_LEV_Pos + 1));
	FLASH->OPTCR1 &= ~(1 << FLASH_OPTCR_BOR_LEV_Pos);
	FLASH->OPTCR  &= ~(1 << (FLASH_OPTCR_BOR_LEV_Pos + 1));
	FLASH->OPTCR  &= ~(1 << FLASH_OPTCR_BOR_LEV_Pos);
	//ReadOut protection level 1 (0xAAxx level 0, 0xCCxx level 2, different value level 1 [selected 0x00xx])
//	FLASH->OPTCR1 &= ~ 0x0000FF00;
//	FLASH->OPTCR &= ~ 0x0000FF00;
	//Check flash memory is not busy and set the option start bit
	FLASH->OPTCR |= FLASH_OPTCR_OPTSTRT;
	while((FLASH->SR & FLASH_SR_BSY) ==  FLASH_SR_BSY);
	//Read dual flash bank Option bit
	if((FLASH->OPTCR & FLASH_OPTCR_BFB2) == FLASH_OPTCR_BFB2)
		flashBank = 2;
	else
		flashBank = 1;
	//Lock memory to prevent access to option bytes and the rest of the flash memory
	HAL_FLASH_OB_Lock();
	HAL_FLASH_Lock();
}

/*	Access to option bytes to change the status of the dual boot byte
 * 	No Parameters:
 * 	No Return:
 */
static void ChangeBootingBank(void){
	//Allow access to flash control registers and option bytes sector
	HAL_FLASH_Unlock();
	HAL_FLASH_OB_Unlock();
	//Read dual flash bank Option bit and set/reset it
	while((FLASH->SR & FLASH_SR_BSY) ==  FLASH_SR_BSY);
	if((FLASH->OPTCR1 & FLASH_OPTCR_BFB2) != FLASH_OPTCR_BFB2)
		FLASH->OPTCR1 |= FLASH_OPTCR_BFB2;
	else
		FLASH->OPTCR1 -= FLASH_OPTCR_BFB2;
	if((FLASH->OPTCR & FLASH_OPTCR_BFB2) != FLASH_OPTCR_BFB2)
		FLASH->OPTCR |= FLASH_OPTCR_BFB2;
	else
		FLASH->OPTCR -= FLASH_OPTCR_BFB2;
	FLASH->OPTCR |= FLASH_OPTCR_OPTSTRT;
	while((FLASH->SR & FLASH_SR_BSY) ==  FLASH_SR_BSY);
	//Lock memory to prevent access to option bytes and the rest of the flash memory
	HAL_FLASH_OB_Lock();
	HAL_FLASH_Lock();
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void ErrorHandler(uint8_t errorCode){
	if(errorCode == CAN_CONFIG_ERROR_CMD){
		while(CAN_Config()!=0){
			HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
			osDelay(500);
		}
	}
	else if(errorCode == CAN_ERROR_CMD)
		HAL_NVIC_SystemReset();
	else if(errorCode == CONFIG_ERROR_CMD)
		HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
	else if((errorCode == FL_ERROR_WR_ADD_RUNNING)||(errorCode == FL_ERROR_NO_CORRECT_BANK)||(errorCode == FL_ERROR_ERASING)||(errorCode == FL_ERROR_WRITING)||
			(errorCode == FL_ERROR_CHECKSUM)||(errorCode == FL_ERROR_RD_ADD_RUNNING)||(errorCode == FL_ERROR_CHANGE_BANK)||(errorCode == FL_ERROR_BINARY_FILE)||
			(errorCode == FL_ERROR_NO_EXIST_OLD_FW)||(errorCode == FL_ERROR_TIMEOUT)){
		if(errorCode != FL_ERROR_ERASING){
			if(flashBank == 1){
				if(FLASH_If_EraseBank(FLASH_BANK_2) != 0){ ErrorHandler(FL_ERROR_ERASING);}
			}
			else if(flashBank == 2){
				if(FLASH_If_EraseBank(FLASH_BANK_1) != 0){ ErrorHandler(FL_ERROR_ERASING);}
			}
		}
		sendResponse(FRIDGE_CMD, FLASH_MEMORY_ERROR_CMD, errorCode, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
		osDelay(500);
		HAL_NVIC_SystemReset();
	}
	else if(errorCode == CSS_ERROR_CMD)
		sendResponse(FRIDGE_CMD, CSS_ERROR_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, ZERO_CMD, END_CMD);
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void){ }

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
