#ifndef CANMANAGER_H_
#define CANMANAGER_H_

/* Includes ------------------------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx_hal.h"

/* Exported functions ------------------------------------------------------------------*/
uint8_t CAN_Config(void);
uint8_t CAN_SendData(uint8_t* data, uint8_t len);
uint8_t CAN_ReceiveData(void);
uint8_t CAN_GetIdFromReceivedMessage(void);
uint8_t CAN_GetReceivedMessage(uint8_t id);
void CAN_SetTransmissionStandarID(uint8_t id);
uint8_t CAN_GetTransmissionStandarID(void);

/* Exported defines ------------------------------------------------------------------*/
#define ACD_CAN_ID					0x10
#define AMS_1_CAN_ID				0x12
#define AMS_2_CAN_ID				0x13
#define AMS_3_CAN_ID				0x14
#define AMS_4_CAN_ID				0x15
#define ABPH_CAN_ID					0x18
#define AIRLOCK_CAN_ID 				0x19
#define FRIDGE_CAN_ID				0x1A
//
#define CAN_MSG_LENGTH 				8
#define CAN_ID_MASK					0xFFF
#define CAN_AMS_ID_MASK				0xFF8
#define CAN_BUFFER_LENGTH			20
#define CAN_RX_MSG_RECEIVED_SG		0x01

/* Definition for CAN port clock resources and pins definition */
//#define CAN2_RX_INT					0
#define CAN_PORT					CAN1
#define CAN2_PORT_CLK_ENABLE		__HAL_RCC_CAN2_CLK_ENABLE
#define CAN2_PORT_CLK_DISABLE		__HAL_RCC_CAN2_CLK_DISABLE
#define CAN1_PORT_CLK_ENABLE		__HAL_RCC_CAN1_CLK_ENABLE
#define CAN1_PORT_CLK_DISABLE		__HAL_RCC_CAN1_CLK_DISABLE
#define CAN2_TX_PIN					GPIO_PIN_13
#define CAN2_RX_PIN					GPIO_PIN_12
#define CAN2_GPIO_PORT				GPIOB
#define CAN2_PORT_GPIO_CLK_EN		__HAL_RCC_GPIOB_CLK_ENABLE
#define CAN1_TX_PIN					GPIO_PIN_9
#define CAN1_RX_PIN					GPIO_PIN_8
#define CAN1_GPIO_PORT				GPIOB
#define CAN1_PORT_GPIO_CLK_EN		__HAL_RCC_GPIOA_CLK_ENABLE

#ifdef CAN2_RX_INT
	#define CANx_RX0_IRQHandler		CAN2_RX0_IRQHandler
	#define CANx_RX1_IRQHandler		CAN2_RX1_IRQHandler
#else
	#define CANx_RX0_IRQHandler		CAN1_RX0_IRQHandler
	#define CANx_RX1_IRQHandler		CAN1_RX1_IRQHandler
#endif

#endif /* CANMANAGER_H_ */
