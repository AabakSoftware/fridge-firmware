/* Includes ------------------------------------------------------------------*/
#include "i2cManager.h"
#include "main.h"
#include "cmsis_os.h"
#include "math.h"

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef I2C_handle;
uint8_t firstRowTemp[] = "TT:xx.x C   MODE";
uint8_t secondRowTemp[] = "CT:xx.x C   mmmm";
uint8_t frostDetectConfigMsg[2] = {0x80, 0x00};
uint8_t humidityConfig[] = {HDC1080_CONFIG_REG, HDC1080_HRES_14BITS|HDC1080_TRES_14BITS|HDC1080_BTST_HV|HDC1080_SERIAL_READ_MODE|HDC1080_HEATER_DIS, HDC1080_CONFIG_B0};

/* Private functions declaration ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/

/*  I2C configuration port to communicate with the MVS and to control the VAT valves
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t i2cConfig(void){
	I2C_handle.Instance 				= I2Cx;
	I2C_handle.Init.AddressingMode 		= I2C_ADDRESSINGMODE_7BIT;
	I2C_handle.Init.ClockSpeed			= 400000;
	I2C_handle.Init.DualAddressMode 	= I2C_DUALADDRESS_DISABLE;
	I2C_handle.Init.DutyCycle 			= I2C_DUTYCYCLE_2 ;
	I2C_handle.Init.GeneralCallMode 	= I2C_GENERALCALL_DISABLE;
	I2C_handle.Init.NoStretchMode 		= I2C_NOSTRETCH_DISABLE;
	I2C_handle.Init.OwnAddress1 		= I2C_MASTER_ADDR;
	I2C_handle.Init.OwnAddress2 		= 0;
	if (HAL_I2C_Init(&I2C_handle) != HAL_OK) { return 1;}
	// Configure Analog filter
	if (HAL_I2CEx_ConfigAnalogFilter(&I2C_handle, I2C_ANALOGFILTER_ENABLE) != HAL_OK){ return 1;}
	// Configure Digital filter
	if (HAL_I2CEx_ConfigDigitalFilter(&I2C_handle, 0) != HAL_OK){ return 1;}

	return 0;
}

/*  Display configuration to work with both lines
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t lcdConfig(void){
	/*		To configure LCD send:
	 * 		 	Start(Master) + Write Device address byte (Master) + ACK (Slave) + frostDetectConfig (Master) + ACK (Slave) + Stop (Master)
	 */
	//set function
	osDelay(50);
	frostDetectConfigMsg [1] = LCD_SET_DISPLAY_FUNC_CONFIG|LCD_2_LINE_MODE|LCD_5_11_STYLE_MODE;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)LCD_ADDRESS, (uint8_t*)frostDetectConfigMsg, 2, 100) != HAL_OK){ return 1;}
	osDelay(2);
	//Display ON
	if(lcdDisplayOn() != 0){return 1;}
	osDelay(2);
	//Display clear
	if(lcdClear() != 0){ return 1;}
	osDelay(2);
	//Configure mode
	frostDetectConfigMsg[1] = LCD_SET_ENTRY_CONFIG|LCD_ENTRY_LEFT|LCD_ENTRY_DECREMENT;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)LCD_ADDRESS, (uint8_t*)frostDetectConfigMsg, 2, 100) != HAL_OK){ return 1;}
	osDelay(2);
	return 0;
}

/*  Clean info in the LCD display
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t lcdClear(void){
	frostDetectConfigMsg[1] = LCD_DISPLAY_CLEAR;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)LCD_ADDRESS, (uint8_t*)frostDetectConfigMsg, 2, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Reset cursor in LCD display
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t lcdResetCursor(void){
	frostDetectConfigMsg[1] = LCD_CURSOR_RETURN;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)LCD_ADDRESS, (uint8_t*)frostDetectConfigMsg, 2, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Turn on display
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t lcdDisplayOn(void){
	frostDetectConfigMsg[1] = LCD_SET_DISPLAY_CONFIG | LCD_DISPLAY_ON;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)LCD_ADDRESS, (uint8_t*)frostDetectConfigMsg, 2, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Turn off display
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t lcdDisplayOff(void){
	frostDetectConfigMsg[1] = LCD_SET_DISPLAY_CONFIG | LCD_DISPLAY_OFF;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)LCD_ADDRESS, (uint8_t*)frostDetectConfigMsg, 2, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Write the required information in the display
 * 	Parameters:
 * 		frostDetectConfig:	resources to write in the screen
 * 		length:	amount of byte to be written
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t lcdDisplayWrite(uint8_t* frostDetectConfig, uint8_t length){
	uint8_t d[] = {0x40,  0x00};
	for(int i=0; i<length; i++){
		d[1] = frostDetectConfig[i];
		if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)LCD_ADDRESS, d, 2, 100) != HAL_OK)
			return 1;
		osDelay(1);
	}
	return 0;
}

/*  Position the cursor in the required location
 * 	Parameters:
 * 		pos: position in the line where to place the cursor
 * 		row: line to place the cursor
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t lcdSetCursor(uint8_t pos, uint8_t row){
	if(row == 0)
		frostDetectConfigMsg[1] = pos | 0x80;
	else if (row == 1)
		frostDetectConfigMsg[1] = pos | 0xC0;
	else
		return 1;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)LCD_ADDRESS, (uint8_t*)frostDetectConfigMsg, 2, 100) != HAL_OK){ return 1;}
	return 0;
}

/*  Configure current temperature information to show it in the display
 * 	Parameters:
 * 		targetTemp:	 refrigerator target temperature
 * 		averageTemp: refrigerator average walls temperature
 * 		mode: 		 refrigerator running mode
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t lcdDisplayTemp(uint32_t targetTemp, uint32_t averageTemp, uint8_t mode, uint8_t status, uint8_t step){
	// write target temperature line in the screen
	if(targetTemp >= 1000){
		firstRowTemp[3] = targetTemp/1000 + 0x30;
		targetTemp -= (targetTemp/1000) * 1000;
	}
	else{ firstRowTemp[3] = ' ';}

	if(targetTemp >= 100){
		firstRowTemp[4] = targetTemp/100 + 0x30;
		targetTemp -= (targetTemp/100) * 100;
	}
	else{ firstRowTemp[4] = '0';}

	if(targetTemp >= 10){
		firstRowTemp[6] = targetTemp/10 + 0x30;
		targetTemp -= (targetTemp/10) * 10;
	}
	else{ firstRowTemp[6] = '0';}

	if(status == LOCK_FRIDGE){
		firstRowTemp[10] = ' ';
		firstRowTemp[11] = ' ';
		firstRowTemp[12] = 'L';
		firstRowTemp[13] = 'O';
		firstRowTemp[14] = 'C';
		firstRowTemp[15] = 'K';
	}
	else if(status == UNLOCK_FRIDGE){
		firstRowTemp[10] = 'U';
		firstRowTemp[11] = 'N';
		firstRowTemp[12] = 'L';
		firstRowTemp[13] = 'O';
		firstRowTemp[14] = 'C';
		firstRowTemp[15] = 'K';
	}

	if(lcdSetCursor(0,0) != 0){ return 1;}
	if(lcdDisplayWrite(firstRowTemp, 16) != 0){ return 1;}

	// write average temperature line in the screen
	if(averageTemp >= 1000){
		secondRowTemp[3] = averageTemp/1000 + 0x30;
		averageTemp -= (averageTemp/1000) * 1000;
	}
	else{ secondRowTemp[3] = ' ';}

	if(averageTemp >= 100){
		secondRowTemp[4] = averageTemp/100 + 0x30;
		averageTemp -= (averageTemp/100) * 100;
	}
	else{ secondRowTemp[4] = '0';}

	if(averageTemp >= 10){
		secondRowTemp[6] = averageTemp/10 + 0x30;
		averageTemp -= (averageTemp/10) * 10;
	}
	else{ secondRowTemp[6] = '0';}

	if(mode == OFF_MODE){
		secondRowTemp[11] = 'O';
		secondRowTemp[12] = 'F';
		secondRowTemp[13] = 'F';
		secondRowTemp[14] = ' ';
	}
	else if(mode == COOLING_MODE){
		secondRowTemp[11] = 'C';
		secondRowTemp[12] = 'O';
		secondRowTemp[13] = 'O';
		secondRowTemp[14] = 'L';
	}
	else if(mode == DEFROST_MODE){
		secondRowTemp[11] = 'D';
		secondRowTemp[12] = 'E';
		secondRowTemp[13] = 'F';
		secondRowTemp[14] = 'R';
	}
	secondRowTemp[15] = step + 0x30;
	if(lcdSetCursor(0,1) != 0){ return 1;}
	if(lcdDisplayWrite(secondRowTemp, 16) != 0){ return 1;}

	//no errors
	return 0;
}

/*  Configure the humidity sensor to use maximum reading resolution
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t humiditySensorConfig(void){
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)HDC1080_WRITE_ADDR, humidityConfig, 3, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Turn on heater of humidity sensor
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t humiditySensorEnableHeater(void){
	humidityConfig[1] |= HDC1080_HEATER_EN;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)HDC1080_WRITE_ADDR, humidityConfig, 3, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Turn off heater of humidity sensor
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t humiditySensorDisableHeater(void){
	humidityConfig[1] &= (~HDC1080_HEATER_EN);
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)HDC1080_WRITE_ADDR, humidityConfig, 3, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Configure sequential read in humidity sensor
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t humiditySensorSequentialReadConfig(void){
	humidityConfig[1] |= HDC1080_SEQUENTIAL_READ_MODE;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)HDC1080_WRITE_ADDR, humidityConfig, 3, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Configure sequential read in humidity sensor
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t humiditySensorSerialReadConfig(void){
	humidityConfig[1] &= (~HDC1080_SEQUENTIAL_READ_MODE);
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)HDC1080_WRITE_ADDR, humidityConfig, 3, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Prepare the sensor to read temperature
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t humiditySensorRequestTempRead(void){
	uint8_t d[] = {HDC1080_TEMP_REG};
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)HDC1080_WRITE_ADDR, d, 1, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Prepare the humidity sensor to read relative humidity
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t humiditySensorRequestHumidityRead(void){
	uint8_t d[] = {HDC1080_HUMIDITY_REG};
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)HDC1080_WRITE_ADDR, d, 1, 100) != HAL_OK)
		return 1;
	return 0;
}

/*  Read requested sensor value from the humidity sensor
 * 	Parameters: None
 * 	Return:
 *  	Other value: temperature information
 *  	0xffff: reading error
 */
uint32_t humiditySensorReadSerialValue(void){
	uint8_t d[] = {0,0};
	if(HAL_I2C_Master_Receive(&I2C_handle, (uint16_t)HDC1080_READ_ADDR, d, 2, 100) != HAL_OK)
		return 0xFFFF;
	return (uint32_t)(d[1] | (d[0] << 8));
}

/*  Read both sensor values from the humidity sensor
 * 	Parameters: None
 * 	Return:
 *  	Other value: temperature information
 *  	0xffff: reading error
 */
uint32_t humiditySensorReadSequentialValue(void){
	uint8_t d[] = {0,0,0,0};
	if(HAL_I2C_Master_Receive(&I2C_handle, (uint16_t)HDC1080_READ_ADDR, d, 4, 100) != HAL_OK)
		return 0xFFFF;
	return (uint32_t)(d[1] | (d[0] << 8) | (d[3] << 16) | (d[2] << 24));
}

/*  Frost sensor configuration
 * 	Parameters: None
 * 	Return:
 *  	0: no error
 *  	1: configuration error
 */
uint8_t frostSensorConfig(void){
	uint8_t frostDetectConfig[] = {0,0,0};
//	uint8_t frostDetectData[] = {0,0};

	//#### 0. read ID from sensor
//	frostDetectConfig[0] = FDC2212_DEVICE_ID_REG;
//	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 1, 100) != HAL_OK){ return 1;}
//	else{
//		if(HAL_I2C_Master_Receive(&I2C_handle, (uint16_t)FDC2212_READ_ADDR, frostDetectData, 2, 100) != HAL_OK){ return 1;}
//		else if((frostDetectData[0] << 8) + frostDetectData[1] != FDC2212_DEVICE_ID){ return 1;}
//	}
//	osDelay(1);

	//#### 1. Clock divider configuration
	frostDetectConfig[0] = FDC2212_CLKDIV_CH0_REG;
	frostDetectConfig[1] = 0x20;	//reserved[00], input frequency selection [01], reserved[00],
	frostDetectConfig[2] = 0x01;	//ch fref divider [xx xxxx xxxx]
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);
	frostDetectConfig[0] = FDC2212_CLKDIV_CH1_REG;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);

	//#### 2. drive current configuration
	frostDetectConfig[0] = FDC2212_DRIVECURRENT_CH0_REG;
	frostDetectConfig[1] = 0x70;	//drive : 1.006mA during reading, oscillation amplitude 1.2-1.8V
	frostDetectConfig[2] = 0x00;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);
	frostDetectConfig[0] = FDC2212_DRIVECURRENT_CH1_REG;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);

	//#### 3. Clock settle count configuration
	frostDetectConfig[0] = FDC2212_SETTLECOUNT_CH0_REG;
	frostDetectConfig[1] = 0x0A;
	frostDetectConfig[2] = 0x00;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);
	frostDetectConfig[0] = FDC2212_SETTLECOUNT_CH1_REG;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);

	//#### 4. Reference count conversion
	frostDetectConfig[0] = FDC2212_RCOUNT_CH0_REG;
	frostDetectConfig[1] = (uint8_t)(FDC2212_COUNT >> 8);
	frostDetectConfig[2] = (uint8_t)FDC2212_COUNT;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);
	frostDetectConfig[0] = FDC2212_RCOUNT_CH1_REG;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);

	//#### 5. Offset configuration
//	frostDetectConfig[0] = FDC2212_OFFSET_CH0_REG;
//	frostDetectConfig[1] = 0x00;
//	frostDetectConfig[2] = 0x00;
//	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
//	osDelay(1);
//	frostDetectConfig[0] = FDC2212_OFFSET_CH1_REG;
//	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
//	osDelay(1);

	//#### 6. Error configuration
//	frostDetectConfig[0] = FDC2212_STATUS_CONFIG_REG;
//	frostDetectConfig[1] = 0x38;	//reserved [00], watchdog EN [1], High amplitude error EN [1], Low amplitude error EN [1], reserved[000 0]
//	frostDetectConfig[2] = 0x00; //watchdog INT EN [0], reserved[0000], frostDetectConfig ready INT EN [0]
//	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
//	osDelay(1);

	//#### 7. MUX configuration
	frostDetectConfig[0] = FDC2212_MUX_CONFIG_REG;
	frostDetectConfig[1] = 0x82;	//Auto scan EN, sequence [0,0], reserved[00 0100 0001] , deglitch frequency filter 10MHz [101]
	frostDetectConfig[2] = 0x0D;
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);

	//#### 8. Address configuration
	frostDetectConfig[0] = FDC2212_CONFIG_REG;
	frostDetectConfig[1] = 0x16;	//active ch [00], sleep mode en [0], reserved [1], sensor current activation [0], reserved [1], clk reference [1], reserved [0]
	frostDetectConfig[2] = 0x01;	//disable INT [0], high current drive EN [0], reserved [00 0001]
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, frostDetectConfig, 3, 100) != HAL_OK){ return 1;}
	osDelay(1);

	return 0;
}

/*  Frost sensor read channel value
 * 	Parameters: None
 * 	Return:
 *  	Other value: capacitance information
 *  	0xffff: reading error
 */
uint32_t frostSensorReadValue(uint8_t sensorID){
	uint8_t lsbData [2] = {0,0};
	uint8_t msbData [2] = {0,0};
	uint8_t lsbAddrData [] = {0};
	uint8_t msbAddrData [] = {0};
	uint8_t statusReg [] = {FDC2212_STATUS_REG}, statusData [] = {0, 0}, sensorStatus = 0;
	//read ready
	if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, statusReg, 1, 100) != HAL_OK){ return 0xffff;}
	else{
		if(HAL_I2C_Master_Receive(&I2C_handle, (uint16_t)FDC2212_READ_ADDR, statusData, 2, 100) != HAL_OK){ return 0xffff;}
		else{
			if(((statusData[1] & 0x08) && (sensorID == 0)) || ((statusData[1] & 0x04) && (sensorID == 1))){ sensorStatus = 1;}
			else { sensorStatus = 0;}
		}
	}

	//configure address
	if(sensorStatus == 1){
		if(sensorID == 0){
			msbAddrData[0] = FDC2212_DATA_CH0_MSB_REG;
			lsbAddrData[0] = FDC2212_DATA_CH0_LSB_REG;
		}
		else if(sensorID == 1){
			msbAddrData[0] = FDC2212_DATA_CH1_MSB_REG;
			lsbAddrData[0] = FDC2212_DATA_CH1_LSB_REG;
		}
		//read information
		if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, msbAddrData, 1, 100) != HAL_OK){ return 0xffff;}
		else{
			if(HAL_I2C_Master_Receive(&I2C_handle, (uint16_t)FDC2212_READ_ADDR, msbData, 2, 100) != HAL_OK){ return 0xffff;}
			else{
				if(HAL_I2C_Master_Transmit(&I2C_handle, (uint16_t)FDC2212_WRITE_ADDR, lsbAddrData, 1, 100) != HAL_OK){ return 0xffff;}
				else{
					if(HAL_I2C_Master_Receive(&I2C_handle, (uint16_t)FDC2212_READ_ADDR, lsbData, 2, 100) != HAL_OK){ return 0xffff;}
				}
			}
		}
		//frostDetectConfig conversion
		if(((msbData[0] & 0x20) == 0x20) || ((msbData[0] & 0x10) == 0x10)) //WD timeout 0x20, Amplitude error 0x10
			return 0xffff;
		else{
			double aux = (uint32_t)(((msbData[0] & 0x0F) << 24) | (msbData[1] << 16) | (lsbData[0] << 8) | lsbData[1]);
			aux = ((double)FDC2212_CH_FIN_SELECT * FDC2212_FREF * (double)aux) / pow(2,28);
			aux = (1/(FDC2212_L * pow(2*PI*aux, 2))) * pow(10,12);
			if(aux < 0){ aux = 0;}
			return (uint32_t)(aux*100); 	//[pF * 100]
		}
	}
	else{ return 0xffff;}
}
