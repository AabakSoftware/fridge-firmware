/* Includes ------------------------------------------------------------------*/
#include "canManager.h"

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef 	  canHandler;
CAN_TxHeaderTypeDef   TxHeader;
CAN_RxHeaderTypeDef   RxHeader;
uint32_t              TxMailbox;
uint8_t 			  data[CAN_MSG_LENGTH];

/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/

/*	CAN1 initialization function
 * 	No parameters:
 * 	Return:
 * 		0: no errors happen
 * 		1: port initialization error
 * 		2: filter configuration error
 * 		3: peripheral start error
 * 		4: activate reading interrupts error
 */
uint8_t CAN_Config(void){
	// ##-1- Configure CAN peripheral
	/*    Baud rate 400kb/s: CAN_BS1_4TQ, CAN_BS2_2TQ, CAN_SJW_1TQ, Prescaler = 15
	*	Baud rate 250kb/s: CAN_BS1_4TQ, CAN_BS2_3TQ, CAN_SJW_1TQ, Prescaler = 21
	*	Baud rate 500kb/s: CAN_BS1_4TQ, CAN_BS2_2TQ, CAN_SJW_1TQ, Prescaler = 12
	*
	* 	BR = APB1_CLK_Freq / (Prescaler * (1 + TQ_BS1 + TQ_BS2))
	*
	* 	For F429ZI:		APB1_CLK_Freq = 42MHz
	*/
	canHandler.Instance 					= CAN_PORT;
	canHandler.Init.TimeTriggeredMode 		= DISABLE;
	canHandler.Init.AutoBusOff				= DISABLE;
	canHandler.Init.AutoWakeUp 				= DISABLE;
	canHandler.Init.AutoRetransmission 		= DISABLE;
	canHandler.Init.ReceiveFifoLocked 		= DISABLE;
	canHandler.Init.TransmitFifoPriority 	= DISABLE;
	canHandler.Init.Mode 					= CAN_MODE_NORMAL;
	canHandler.Init.SyncJumpWidth 			= CAN_SJW_1TQ;
	canHandler.Init.TimeSeg1 				= CAN_BS1_4TQ;
	canHandler.Init.TimeSeg2 				= CAN_BS2_2TQ;
	canHandler.Init.Prescaler 				= 12;
	if (HAL_CAN_Init(&canHandler) != HAL_OK){ return 1;}
	// ##-2- Configure the CAN Filter
	CAN_FilterTypeDef  sFilterConfig;
	if(CAN_PORT == CAN1)
		sFilterConfig.FilterBank 			= 0;
	else if(CAN_PORT == CAN2)
		sFilterConfig.FilterBank 			= 14;
	sFilterConfig.FilterMode 				= CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale 				= CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh 				= ACD_CAN_ID << 5;
	sFilterConfig.FilterIdLow 				= 0x0000;
	sFilterConfig.FilterMaskIdHigh 			= CAN_ID_MASK << 5;
	sFilterConfig.FilterMaskIdLow 			= 0x0000;
	sFilterConfig.FilterFIFOAssignment 		= CAN_RX_FIFO0;
	sFilterConfig.FilterActivation 			= ENABLE;
	sFilterConfig.SlaveStartFilterBank 		= 14;
	if(HAL_CAN_ConfigFilter(&canHandler, &sFilterConfig) != HAL_OK){ return 2;}
	// ##-3- Start the CAN peripheral
	if (HAL_CAN_Start(&canHandler) != HAL_OK){ return 3;}
	// ##-4- Activate CAN RX notification
	if (HAL_CAN_ActivateNotification(&canHandler, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK){  return 4;}
	//Initialize transmission buffer characteristics
	TxHeader.StdId 					= FRIDGE_CAN_ID;
	TxHeader.RTR 					= CAN_RTR_DATA;
	TxHeader.IDE 					= CAN_ID_STD;
	TxHeader.DLC 					= CAN_MSG_LENGTH;
	TxHeader.TransmitGlobalTime 	= DISABLE;
	// Return successful value
	return 0;
}

/*	Send data to the CAN communication port, max data 8 bytes
 * 	Parameters:
 * 		data to send in a byte array shape
 * 		length of the byte array
 * 	Return:
 * 		0: no errors happen
 * 		1: wrong data dimension
 * 		2: error in data transmission process
 */
uint8_t CAN_SendData(uint8_t* data, uint8_t len){
	// Wrong data dimension checking
	if((len < 0) || (len > 8)){ return 1;}
	// Request transmission
	if(HAL_CAN_AddTxMessage(&canHandler, &TxHeader, data, &TxMailbox) != HAL_OK) { return 2;}
	// Wait transmission complete
	while(HAL_CAN_GetTxMailboxesFreeLevel(&canHandler) != 3) {}
	// Return successful value
	return 0;
}

/*  Receive data from the CAN communication port
 *  No parameters
 *  Return:
 *  	0: no error happen
 *  	1: no data reception, the FIFO is empty
 *  	2: error reading the received data
 */
uint8_t CAN_ReceiveData(void){
	if(HAL_CAN_GetRxMessage(&canHandler, CAN_RX_FIFO0, &RxHeader, data) != HAL_OK){ return 2;}
	return 0;
}

/* Returns the ID from the device who sent the last message to the CAN network */
uint8_t CAN_GetIdFromReceivedMessage(void){
	return (uint8_t)RxHeader.StdId;
}

/* Returns the received data */
uint8_t CAN_GetReceivedMessage(uint8_t id){
	return data[id];
}

/*  Set the identification code for the selected AMS
 *  Parameters
 *  	id: code of the requested AMS
 *  No Return:
 */
void CAN_SetTransmissionStandarID(uint8_t id){
	TxHeader.StdId = id;
}

/*  Get the identification code for the selected AMS
 *  No parameters
 *  Return:
 *  	AMS CAN identification
 */
uint8_t CAN_GetTransmissionStandarID(void){
	return (uint8_t)TxHeader.StdId;
}
