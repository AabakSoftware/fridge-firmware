################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/adcManager.c \
../Src/canManager.c \
../Src/flashManager.c \
../Src/freertos.c \
../Src/i2cManager.c \
../Src/main.c \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_hal_timebase_tim.c \
../Src/stm32f4xx_it.c \
../Src/syscalls.c \
../Src/system_stm32f4xx.c 

OBJS += \
./Src/adcManager.o \
./Src/canManager.o \
./Src/flashManager.o \
./Src/freertos.o \
./Src/i2cManager.o \
./Src/main.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_hal_timebase_tim.o \
./Src/stm32f4xx_it.o \
./Src/syscalls.o \
./Src/system_stm32f4xx.o 

C_DEPS += \
./Src/adcManager.d \
./Src/canManager.d \
./Src/flashManager.d \
./Src/freertos.d \
./Src/i2cManager.d \
./Src/main.d \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_hal_timebase_tim.d \
./Src/stm32f4xx_it.d \
./Src/syscalls.d \
./Src/system_stm32f4xx.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F429xx -I"D:/FW/SW4STM32workspace/Fridge_MK4_uC_v2.0/Inc" -I"D:/FW/SW4STM32workspace/Fridge_MK4_uC_v2.0/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/FW/SW4STM32workspace/Fridge_MK4_uC_v2.0/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/FW/SW4STM32workspace/Fridge_MK4_uC_v2.0/Middlewares/Third_Party/FreeRTOS/Source/include" -I"D:/FW/SW4STM32workspace/Fridge_MK4_uC_v2.0/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"D:/FW/SW4STM32workspace/Fridge_MK4_uC_v2.0/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"D:/FW/SW4STM32workspace/Fridge_MK4_uC_v2.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/FW/SW4STM32workspace/Fridge_MK4_uC_v2.0/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


