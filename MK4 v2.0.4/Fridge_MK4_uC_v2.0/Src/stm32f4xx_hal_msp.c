/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "canManager.h"
#include "adcManager.h"
#include "i2cManager.h"

/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef 	adcDmaHandle;

/**
  * Initializes the Global MSP.
  */
void HAL_MspInit(void){
	__HAL_RCC_SYSCFG_CLK_ENABLE();
	__HAL_RCC_PWR_CLK_ENABLE();
	/* System interrupt init*/
	/* PendSV_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(PendSV_IRQn, 15, 0);
}

/*	ADC clock and pin initialization callback
 * 	Parameters: pointer to ADC handler
 * 	No Return:
 */
void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc){
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(hadc->Instance == ADC_MODULE){
		//##-1- Enable peripherals
		ADC_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_GPIOC_CLK_ENABLE();

		//##-2- Configure peripheral GPIOs
	    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    //
	    GPIO_InitStruct.Pin = RIGHT_WALL_TEMP_Pin;
	    HAL_GPIO_Init(RIGHT_WALL_TEMP_GPIO_Port, &GPIO_InitStruct);
	    //
	    GPIO_InitStruct.Pin = LEFT_WALL_TEMP_Pin;
	    HAL_GPIO_Init(LEFT_WALL_TEMP_GPIO_Port, &GPIO_InitStruct);
	    //
	    GPIO_InitStruct.Pin = PCB_TEMP_Pin;
	    HAL_GPIO_Init(PCB_TEMP_GPIO_Port, &GPIO_InitStruct);
	    //
		GPIO_InitStruct.Pin = SPARE_1_TEMP_Pin;
		HAL_GPIO_Init(SPARE_1_TEMP_GPIO_Port, &GPIO_InitStruct);
		//
		GPIO_InitStruct.Pin = SPARE_2_TEMP_Pin;
		HAL_GPIO_Init(SPARE_2_TEMP_GPIO_Port, &GPIO_InitStruct);
		//
		GPIO_InitStruct.Pin = SPARE_3_TEMP_Pin;
		HAL_GPIO_Init(SPARE_3_TEMP_GPIO_Port, &GPIO_InitStruct);
		//
		GPIO_InitStruct.Pin = EVAPORATOR_TEMP_Pin;
		HAL_GPIO_Init(EVAPORATOR_TEMP_GPIO_Port, &GPIO_InitStruct);
		//
		GPIO_InitStruct.Pin = DEFROST_HEATER_TEMP_1_Pin;
		HAL_GPIO_Init(DEFROST_HEATER_TEMP_1_GPIO_Port, &GPIO_InitStruct);
		//
		GPIO_InitStruct.Pin = DEFROST_HEATER_TEMP_2_Pin;
		HAL_GPIO_Init(DEFROST_HEATER_TEMP_2_GPIO_Port, &GPIO_InitStruct);

		//##-3- DMA initialization
		adcDmaHandle.Instance 					= ADC_DMA_STREAM;
		adcDmaHandle.Init.Channel				= ADC_DMA_CHANNEL;
		adcDmaHandle.Init.Direction 			= DMA_PERIPH_TO_MEMORY;
		adcDmaHandle.Init.PeriphInc 			= DMA_PINC_DISABLE;
		adcDmaHandle.Init.MemInc 				= DMA_MINC_ENABLE;
		adcDmaHandle.Init.PeriphDataAlignment 	= DMA_PDATAALIGN_WORD;
		adcDmaHandle.Init.MemDataAlignment 		= DMA_MDATAALIGN_WORD;
		adcDmaHandle.Init.Mode 					= DMA_CIRCULAR;
		adcDmaHandle.Init.Priority 				= DMA_PRIORITY_LOW;
		if (HAL_DMA_Init(&adcDmaHandle) != HAL_OK){  Error_Handler();}

		__HAL_LINKDMA(hadc, DMA_Handle, adcDmaHandle);
	}
}

/*	ADC clock and pin deinitialization callback
 * 	Parameters: pointer to ADC handler
 * 	No Return:
 */
void HAL_ADC_MspDeInit(ADC_HandleTypeDef* hadc){
	if(hadc->Instance == ADC_MODULE){
		// Peripheral clock disable
		ADC_CLK_DISABLE();
		//##-1- Disable peripherals ################################
		HAL_GPIO_DeInit(RIGHT_WALL_TEMP_GPIO_Port, RIGHT_WALL_TEMP_Pin);
		HAL_GPIO_DeInit(LEFT_WALL_TEMP_GPIO_Port, LEFT_WALL_TEMP_Pin);
		HAL_GPIO_DeInit(PCB_TEMP_GPIO_Port, PCB_TEMP_Pin);
		HAL_GPIO_DeInit(SPARE_1_TEMP_GPIO_Port, SPARE_1_TEMP_Pin);
		HAL_GPIO_DeInit(SPARE_2_TEMP_GPIO_Port, SPARE_2_TEMP_Pin);
		HAL_GPIO_DeInit(SPARE_3_TEMP_GPIO_Port, SPARE_3_TEMP_Pin);
		HAL_GPIO_DeInit(EVAPORATOR_TEMP_GPIO_Port, EVAPORATOR_TEMP_Pin);
		HAL_GPIO_DeInit(DEFROST_HEATER_TEMP_1_GPIO_Port, DEFROST_HEATER_TEMP_1_Pin);
		HAL_GPIO_DeInit(DEFROST_HEATER_TEMP_2_GPIO_Port, DEFROST_HEATER_TEMP_2_Pin);

		//##-2- Deinitialization of DMA linked to the ADC module
		HAL_DMA_DeInit(hadc->DMA_Handle);
	}
}

/*	CAN port clock and pin initialization callback
 * 	Parameters: pointer to CAN handler
 * 	No Return:
 */
void HAL_CAN_MspInit(CAN_HandleTypeDef* hcan){
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(hcan->Instance == CAN_PORT) {
		if(CAN_PORT == CAN2){
			//##-1- Enable peripherals
			CAN2_PORT_CLK_ENABLE();
			CAN1_PORT_CLK_ENABLE();
			__HAL_RCC_GPIOB_CLK_ENABLE();
			//##-2- Configure peripheral GPIO
			GPIO_InitStruct.Pin = CAN2_TX_PIN|CAN2_RX_PIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF9_CAN2;
			HAL_GPIO_Init(CAN2_GPIO_PORT, &GPIO_InitStruct);
			//##-3- configure reception interruptions
			HAL_NVIC_SetPriority(CAN2_RX0_IRQn, 5, 2);
			HAL_NVIC_EnableIRQ(CAN2_RX0_IRQn);
		}
		else if(CAN_PORT == CAN1){
			//##-1- Enable peripherals
			CAN1_PORT_CLK_ENABLE();
			__HAL_RCC_GPIOA_CLK_ENABLE();
			//##-2- Configure peripheral GPIO
			GPIO_InitStruct.Pin = CAN1_TX_PIN|CAN1_RX_PIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
			HAL_GPIO_Init(CAN1_GPIO_PORT, &GPIO_InitStruct);
			//##-3- configure reception interruptions
			HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 5, 2);
			HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
		}
	}
}

/*	CAN port clock and pin deinitialization callback
 * 	Parameters: pointer to CAN handler
 * 	No Return:
 */
void HAL_CAN_MspDeInit(CAN_HandleTypeDef* hcan){
	if(hcan->Instance == CAN_PORT){
		if(CAN_PORT == CAN2){
			//##-1- Peripheral clock disable
			CAN2_PORT_CLK_DISABLE();
			CAN1_PORT_CLK_DISABLE();
			//##-2- Disable peripherals
			HAL_GPIO_DeInit(CAN2_GPIO_PORT, CAN2_TX_PIN|CAN2_RX_PIN);
			//##-3- Disable the interruption configuration
			HAL_NVIC_DisableIRQ(CAN2_RX0_IRQn);
		}
		else if(CAN_PORT == CAN1){
			//##-1- Peripheral clock disable
			CAN1_PORT_CLK_DISABLE();
			//##-2- Disable peripherals
			HAL_GPIO_DeInit(CAN1_GPIO_PORT, CAN1_TX_PIN|CAN1_RX_PIN);
			//##-3- Disable the interruption configuration
			HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
		}
	}
}

/*	I2C initialization callback
 * 	Parameters: pointer to hardware I2C handler
 * 	No Return:
 */
void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c){
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(hi2c->Instance == I2Cx){
		__HAL_RCC_GPIOB_CLK_ENABLE();
		/* GPIO configuration */
		GPIO_InitStruct.Pin 		= I2C_SDA_Pin;
		GPIO_InitStruct.Mode 		= GPIO_MODE_AF_OD;
		GPIO_InitStruct.Pull 		= GPIO_PULLUP;
		GPIO_InitStruct.Speed 		= GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate 	= I2Cx_GPIO_AF;
		HAL_GPIO_Init(I2C_SDA_GPIO_Port, &GPIO_InitStruct);
		//
		GPIO_InitStruct.Pin 		= I2C_SCL_Pin;
		HAL_GPIO_Init(I2C_SCL_GPIO_Port, &GPIO_InitStruct);
		/* Peripheral clock enable */
		I2Cx_CLK_ENABLE();
		/* Interruptions configuration */
		HAL_NVIC_SetPriority(I2Cx_ER_IRQn, 6, 1);
		HAL_NVIC_EnableIRQ(I2Cx_ER_IRQn);
		HAL_NVIC_SetPriority(I2Cx_EV_IRQn, 6, 2);
		HAL_NVIC_EnableIRQ(I2Cx_EV_IRQn);
	}
}

/*	I2C deinitialization callback
 * 	Parameters: pointer to hardware I2C handler
 * 	No Return:
 */
void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c){
	if(hi2c->Instance == I2Cx){
	    /* Peripheral clock disable */
		I2Cx_CLK_DISABLE();
		/* GPIO disable pins */
		HAL_GPIO_DeInit(I2C_SDA_GPIO_Port, I2C_SDA_Pin);
		HAL_GPIO_DeInit(I2C_SCL_GPIO_Port, I2C_SCL_Pin);
		/* Interrupts disable */
	    HAL_NVIC_DisableIRQ(I2Cx_ER_IRQn);
	    HAL_NVIC_DisableIRQ(I2Cx_EV_IRQn);
	}
}

/*	PWM channels initialization of timers for control the addressable LEDs
 * 	Parameters: pointer to TIM handler
 * 	No Return:
 */
void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm){
	if(htim_pwm->Instance == RECIRC_FAN_TIM){
		RECIRC_FAN_TIM_CLK_ENABLE();
	}
}

/*	Timers GPIO initialization function, is not a callback
 * 	Parameters: pointer to hardware timer handler
 * 	No Return:
 */
void HAL_TIM_MspPostInit(TIM_HandleTypeDef* htim){
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(htim->Instance == RECIRC_FAN_TIM){
		GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull 		= GPIO_NOPULL;
		GPIO_InitStruct.Speed		= GPIO_SPEED_FREQ_LOW;
		GPIO_InitStruct.Alternate 	= RECIRC_FAN_TIM_GPIO_AF;
		//
		GPIO_InitStruct.Pin = RECIRCULATION_FAN_Pin;
		HAL_GPIO_Init(RECIRCULATION_FAN_GPIO_Port, &GPIO_InitStruct);
	}
}

/*	Timers deinitialization callback
 * 	Parameters: pointer to hardware timer handler
 * 	No Return:
 */
void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef* htim_pwm){
	if(htim_pwm->Instance == RECIRC_FAN_TIM){
		RECIRC_FAN_TIM_CLK_DISABLE();
	}
}

/*	Internal timers initialization
 * 	Parameters: pointer to TIM handler
 * 	No Return:
 */
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim_base){
	if(htim_base->Instance == US_TIMER){
		/* Peripheral clock enable */
		US_TIMER_CLK_ENABLE();
		/* TIM12 interrupt Init */
		HAL_NVIC_SetPriority(US_TIMER_IRQn, 5, 1);
		HAL_NVIC_EnableIRQ(US_TIMER_IRQn);
	}
}

/*	Internal timers deinitialization
 * 	Parameters: pointer to TIM handler
 * 	No Return:
 */
void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base){
	if(htim_base->Instance == US_TIMER){
		/* Peripheral clock disable */
		US_TIMER_CLK_DISABLE();
		/* TIM12 interrupt DeInit */
		HAL_NVIC_DisableIRQ(US_TIMER_IRQn);
	}
}
