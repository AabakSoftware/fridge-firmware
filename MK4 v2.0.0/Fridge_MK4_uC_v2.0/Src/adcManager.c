/* Includes ------------------------------------------------------------------*/
#include "adcManager.h"
#include <math.h>

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef adcSensorHandle;
DMA_HandleTypeDef adcDmaHandle;
uint16_t adcValues_Buffer [ADC_SAMPLES][ADC_NUMBER_SENSORS];
uint32_t adcValues[ADC_NUMBER_SENSORS];
uint8_t currentSample = 0, readFlag = 0;
float averageValues[ADC_NUMBER_SENSORS], magnitudeMeasured[ADC_NUMBER_SENSORS];

/* Private functions ---------------------------------------------------------*/
uint8_t ADCSensors_Config(void){
	ADC_ChannelConfTypeDef adcsConfig = {0};

	// Set all the elements of the buffers to 0
	for(uint16_t i=0; i<ADC_SAMPLES; i++){
		for(uint8_t j=0; j<ADC_NUMBER_SENSORS; j++)
			adcValues_Buffer[i][j] = 0x0000;
	}
	for(uint8_t j=0; j<ADC_NUMBER_SENSORS; j++){
		averageValues[j] = 0.0f;
		adcValues[j] = 0;
		magnitudeMeasured[j] = 0.0f;
	}

	// Configure DMA interruptions
	ADC_DMA_CLK_ENABLE();
	HAL_NVIC_SetPriority(ADC_DMA_STREAM_IRQn, 6, 0);
	HAL_NVIC_EnableIRQ(ADC_DMA_STREAM_IRQn);

	// Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
	adcSensorHandle.Instance 						= ADC_MODULE;
	adcSensorHandle.Init.ClockPrescaler 			= ADC_CLOCK_SYNC_PCLK_DIV4;  // ADC_clk = APB2_clk / 4
	adcSensorHandle.Init.Resolution 				= ADC_RESOLUTION_12B;		 // Max resolution
	adcSensorHandle.Init.ScanConvMode 				= ENABLE;
	adcSensorHandle.Init.ContinuousConvMode 		= ENABLE;
	adcSensorHandle.Init.DiscontinuousConvMode 		= DISABLE;
	adcSensorHandle.Init.NbrOfDiscConversion   		= 0;
	adcSensorHandle.Init.ExternalTrigConvEdge 		= ADC_EXTERNALTRIGCONVEDGE_NONE;
	adcSensorHandle.Init.ExternalTrigConv 			= ADC_SOFTWARE_START;	// Use channel 1 of TIM1 to trigger the reading process
	adcSensorHandle.Init.DataAlign 					= ADC_DATAALIGN_RIGHT;
	adcSensorHandle.Init.NbrOfConversion 			= ADC_NUMBER_SENSORS;
	adcSensorHandle.Init.DMAContinuousRequests 		= ENABLE;
	adcSensorHandle.Init.EOCSelection 				= ADC_EOC_SINGLE_CONV;
	if (HAL_ADC_Init(&adcSensorHandle) != HAL_OK){  return 1;}

	//Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time
	adcsConfig.Channel 			= RIGHT_WALL_TEMP_ADC_CH;
	adcsConfig.Rank 			= RIGHT_WALL_TEMP_ID + 1;
	adcsConfig.SamplingTime 	= ADC_SAMPLETIME_480CYCLES;
	adcsConfig.Offset			= 0;
	adcsConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= LEFT_WALL_TEMP_ADC_CH;
	adcsConfig.Rank 		= LEFT_WALL_TEMP_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= PCB_TEMP_ADC_CH;
	adcsConfig.Rank			= PCB_TEMP_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= SPARE_1_TEMP_ADC_CH;
	adcsConfig.Rank 		= SPARE_1_TEMP_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= SPARE_2_TEMP_ADC_CH;
	adcsConfig.Rank 		= SPARE_2_TEMP_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= SPARE_3_TEMP_ADC_CH;
	adcsConfig.Rank 		= SPARE_3_TEMP_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= EVAPORATOR_TEMP_ADC_CH;
	adcsConfig.Rank 		= EVAPORATOR_TEMP_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= DEFROST_HEATER_TEMP_1_ADC_CH;
	adcsConfig.Rank 		= DEFROST_HEATER_TEMP_1_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= DEFROST_HEATER_TEMP_2_ADC_CH;
	adcsConfig.Rank 		= DEFROST_HEATER_TEMP_2_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}
	//
	adcsConfig.Channel 		= UC_TEMP_ACD_CH;
	adcsConfig.Rank 		= UC_TEMP_ID + 1;
	if (HAL_ADC_ConfigChannel(&adcSensorHandle, &adcsConfig) != HAL_OK){ return 2;}

	// Return successful value
	return 0;
}

/*	Function for updating the average ADC value from the sensors
 * 	No parameters:
 * 	No return:
 */
void adcStartDMA(void){
	HAL_ADC_Start_DMA(&adcSensorHandle, adcValues, ADC_NUMBER_SENSORS);
}

/*	Function for updating the average ADC value from the sensors
 * 	No parameters:
 * 	No return:
 */
void updateAverageValue(void){
	float aux = 0.0f;
	/*	(480+12) cycles per measure * 50 measures (average) * 2 devices
	 * 	ADC_clock = PH2_clk / 2 = 21Mhz
	 * 	ADC_freq = 47,62ns
	 */
	if(currentSample < ADC_SAMPLES){
		for(uint8_t id=0; id<ADC_NUMBER_SENSORS; id++){
			adcValues_Buffer[currentSample][id] = adcValues[id];
		}
		currentSample ++;
	}
	else{
//####- Reset ADC channel and do the average
		currentSample = 0;
		for(uint8_t j=0; j<ADC_NUMBER_SENSORS; j++){
			aux = 0.0f;
			for(uint16_t i=0; i<ADC_SAMPLES; i++){
				aux += adcValues_Buffer[i][j];
			}
			aux = aux / (float)ADC_SAMPLES;
			averageValues[j] = aux;
		}
//####- Convert digital voltage readings to a physical magnitude value
		magnitudeMeasured[PCB_TEMP_ID] = ((averageValues[PCB_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE) - PCB_TEMP_VOFF)/PCB_TEMP_M;
		//
		magnitudeMeasured[UC_TEMP_ID] = (((averageValues[UC_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE)-UC_TEMP_VOFF)/UC_TEMP_M) + UC_TEMP_TOFF;
		//
		aux = (averageValues[RIGHT_WALL_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		aux = FRIDGE_TEMP_RPU*aux/(ADC_MAX_VOLT-aux);
		magnitudeMeasured[RIGHT_WALL_TEMP_ID] = (1/(FRIDGE_TEMP_A + FRIDGE_TEMP_B*log(aux/FRIDGE_TEMP_R25) + FRIDGE_TEMP_C*pow(log(aux/FRIDGE_TEMP_R25),2) + FRIDGE_TEMP_D*pow(log(aux/FRIDGE_TEMP_R25),3))) - KELVIN_2_CELSIUS;
//		magnitudeMeasured[RIGHT_WALL_TEMP_ID] = (averageValues[RIGHT_WALL_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		//
		aux = (averageValues[LEFT_WALL_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		aux = FRIDGE_TEMP_RPU*aux/(ADC_MAX_VOLT-aux);
		magnitudeMeasured[LEFT_WALL_TEMP_ID] = (1/(FRIDGE_TEMP_A + FRIDGE_TEMP_B*log(aux/FRIDGE_TEMP_R25) + FRIDGE_TEMP_C*pow(log(aux/FRIDGE_TEMP_R25),2) + FRIDGE_TEMP_D*pow(log(aux/FRIDGE_TEMP_R25),3))) - KELVIN_2_CELSIUS;
//		magnitudeMeasured[LEFT_WALL_TEMP_ID] = (averageValues[LEFT_WALL_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		//
		magnitudeMeasured[SPARE_1_TEMP_ID] = (averageValues[SPARE_1_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		//
		magnitudeMeasured[SPARE_2_TEMP_ID] = (averageValues[SPARE_2_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		//
		magnitudeMeasured[SPARE_3_TEMP_ID] = (averageValues[SPARE_3_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		//
		magnitudeMeasured[EVAPORATOR_TEMP_ID] = (averageValues[EVAPORATOR_TEMP_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		//
		magnitudeMeasured[DEFROST_HEATER_TEMP_1_ID] = (averageValues[DEFROST_HEATER_TEMP_1_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
		//
		magnitudeMeasured[DEFROST_HEATER_TEMP_2_ID] = (averageValues[DEFROST_HEATER_TEMP_2_ID]*ADC_MAX_VOLT/ADC_MAX_VALUE);
	}
}

/*	Function for obtaining the average temperature value
 * 	No parameters:
 * 	Return:
 * 		value of current sample
 */
uint8_t readCurrentSample(void){
	return currentSample;
}

/*	Function for obtaining the average value of the sensor
 * 	Parameters: ID of the read device
 * 	Return:
 * 		value of the average temperature read
 */
uint32_t readSensor(uint8_t id){
	return (uint32_t)(magnitudeMeasured[id]*100);
}

