#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);
void ErrorHandler(uint8_t errorCode);
void sendResponse(uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7);

/* Private defines -----------------------------------------------------------*/
#define FW_VERSION 						1
#define FW_SUBVERSION					0

//###############  Auxiliary LED  ###############
#define DEBUG_LED_Pin 					GPIO_PIN_8
#define DEBUG_LED_GPIO_Port 			GPIOA

//###############  DIGITAL CONTROLLED SYSTEMS  ###############
#define COOLING_FAN_Pin 				GPIO_PIN_7
#define COOLING_FAN_GPIO_Port 			GPIOE
#define RECIRCULATION_FAN_Pin 			GPIO_PIN_9 	//GPIO_PIN_8
#define RECIRCULATION_FAN_GPIO_Port 	GPIOE
#define DOOR_HEATER_EN_Pin 				GPIO_PIN_8	//GPIO_PIN_9
#define DOOR_HEATER_EN_GPIO_Port 		GPIOE
#define LIGHTS_EN_Pin 					GPIO_PIN_10
#define LIGHTS_EN_GPIO_Port 			GPIOE
#define HEATER_EN_Pin 					GPIO_PIN_11
#define HEATER_EN_GPIO_Port 			GPIOE
#define COMPRESSOR_EN_Pin 				GPIO_PIN_12
#define COMPRESSOR_EN_GPIO_Port 		GPIOE
#define ADD_AC_RELAY_1_EN_Pin 			GPIO_PIN_13
#define ADD_AC_RELAY_1_EN_GPIO_Port 	GPIOE
#define ADD_AC_RELAY_2_EN_Pin 			GPIO_PIN_14
#define ADD_AC_RELAY_2_EN_GPIO_Port 	GPIOE
//
#define RECIRC_FAN_TIM					TIM1
#define RECIRC_FAN_TIM_CLK_ENABLE		__HAL_RCC_TIM1_CLK_ENABLE
#define RECIRC_FAN_TIM_CLK_DISABLE		__HAL_RCC_TIM1_CLK_DISABLE
#define RECIRC_FAN_TIM_GPIO_AF			GPIO_AF1_TIM1
#define RECIRC_FAN_TIM_CHANNEL			TIM_CHANNEL_1

//###############  INTERRUPTIONS  ###############
#define AC_ZC_DETECT_Pin 					GPIO_PIN_15
#define AC_ZC_DETECT_GPIO_Port 				GPIOE
#define AC_ZC_DETECT_IRQn					EXTI15_10_IRQn
#define AC_ZC_DETECT_IRQHandler				EXTI15_10_IRQHandler
//
#define DEFROST_BTN_Pin 					GPIO_PIN_2			//GPIO_PIN_0
#define DEFROST_BTN_GPIO_Port 				GPIOD
#define DEFROST_BTN_IRQn					EXTI2_IRQn			//EXTI0_IRQn
#define DEFROST_BTN_IRQHandler				EXTI2_IRQHandler	//EXTI0_IRQHandler
//
#define DECREASE_SET_TEMP_BTN_Pin 			GPIO_PIN_0			//GPIO_PIN_1
#define DECREASE_SET_TEMP_BTN_GPIO_Port 	GPIOD
#define DECREASE_SET_TEMP_BTN_IRQn			EXTI0_IRQn			//EXTI1_IRQn
#define DECREASE_SET_TEMP_BTN_IRQHandler	EXTI0_IRQHandler	//EXTI1_IRQHandler
//
#define INCREASE_SET_TEMP_BTN_Pin 			GPIO_PIN_1			//GPIO_PIN_2
#define INCREASE_SET_TEMP_BTN_GPIO_Port 	GPIOD
#define INCREASE_SET_TEMP_BTN_IRQn			EXTI1_IRQn  		//EXTI2_IRQn
#define INCREASE_SET_TEMP_BTN_IRQHandler	EXTI1_IRQHandler	//EXTI2_IRQHandler

//###############  Microseconds timer  ###############
#define US_TIMER						TIM12
#define US_TIMER_CLK_DISABLE			__HAL_RCC_TIM12_CLK_DISABLE
#define US_TIMER_CLK_ENABLE				__HAL_RCC_TIM12_CLK_ENABLE
#define US_TIMER_IRQn					TIM8_BRK_TIM12_IRQn
#define US_TIMER_IRQn_Handler			TIM8_BRK_TIM12_IRQHandler

//###############  Commands  ###############
#define ACD_CMD							0x10
#define FRIDGE_CMD						0x19
#define PCB_NOTIFICATION_CMD			0x01
#define JAVA_NOTIFICATION_CMD			0x02
//Sequences
#define START_COOLING_SEQ_CMD			0x01
#define START_DEFROST_SEQ_CMD			0x02
#define STOP_SEQ_CMD					0x03
#define SET_TARGET_TEMP_CMD				0x04
#define GET_TARGET_TEMP_CMD				0x05
#define LOCK_BUTTON_PANEL_CMD			0x06
#define UNLOCK_BUTTON_PANEL_CMD			0x07
#define GET_RUN_MODE_CMD				0x08
#define GET_DEFROST_TIMER_VAL_CMD		0x09
//
#define OFF_MODE						0
#define COOLING_MODE					1
#define DEFROST_MODE					2
#define UNLOCK_FRIDGE					0
#define LOCK_FRIDGE						1
//Digital systems
#define DOOR_HEATER_CMD					0x20
#define RECIRCULATION_FAN_CMD			0x21
#define COOLING_FAN_CMD					0x22
#define LIGHTS_EN_CMD					0x23
#define COMPRESSOR_EN_CMD				0x24
#define HEATER_EN_CMD					0x25
#define ADD_AC_RELAY_1_EN_CMD			0x26
#define ADD_AC_RELAY_2_EN_CMD			0x27
//Analog sensors
#define RIGHT_WALL_TEMP_CMD				0x10
#define LEFT_WALL_TEMP_CMD				0x11
#define PCB_TEMP_CMD					0x12
#define EVAPORATOR_TEMP_CMD				0x13
#define DEFROST_HEATER_TEMP_1_CMD		0X14
#define DEFROST_HEATER_TEMP_2_CMD		0X15
#define SPARE_1_TEMP_CMD				0x16
#define SPARE_2_TEMP_CMD				0x17
#define SPARE_3_TEMP_CMD				0x18
#define UC_TEMP_CMD						0x19
#define READ_AVG_TEMP_CMD				0x1A
//Humidity sensor commands
#define GET_HUMIDITY_CMD				0x30
#define GET_HUMIDITY_TEMP_CMD			0x31
#define HUMIDITY_HEATER_ON_CMD			0x32
#define HUMIDITY_HEATER_OFF_CMD			0x33
#define HUMIDITY_SERIAL_READ_CMD		0x34
#define HUMIDITY_SEQ_READ_CMD			0x35
//Frost detector sensor commands
#define GET_FROST_1_CAP_CMD				0x40
#define GET_FROST_2_CAP_CMD				0x41
//OTA commands
#define FW_UPDATE_CMD					0xB0
#define FW_ROLLBACK_CMD					0xB1
#define FW_VERSION_CMD					0xB2
#define FW_BANK_CMD						0xB3
#define FW_RESTART_CMD					0xB4
//Error & notifications commands
#define CONFIG_ERROR_CMD				0xE0
#define CAN_ERROR_CMD					0xE1
#define CAN_CONFIG_ERROR_CMD			0x01
#define I2C_ERROR_CMD					0xE2
#define FLASH_MEMORY_ERROR_CMD			0xE3
#define FL_ERROR_WR_ADD_RUNNING			0x10
#define FL_ERROR_NO_CORRECT_BANK		0x11
#define FL_ERROR_ERASING				0x12
#define FL_ERROR_WRITING				0x13
#define FL_ERROR_CHECKSUM				0x14
#define FL_ERROR_RD_ADD_RUNNING			0x15
#define FL_ERROR_CHANGE_BANK			0x16
#define FL_ERROR_BINARY_FILE			0x17
#define FL_ERROR_NO_EXIST_OLD_FW		0x18
#define FL_ERROR_TIMEOUT				0x19
#define CSS_ERROR_CMD					0xE4
//
#define FRIDGE_RESET_CMD				0x49
#define	IWDG_RESET_CMD					0x01
#define WWDG_RESET_CMD					0x02
#define SW_RESET_CMD					0x03
#define BOR_RESET_CMD					0x04
//Communication commands
#define HB_CMD							0xF0
#define SYNCH_CMD						0xF1
#define ZERO_CMD						0x00
#define START_CMD						0xFE
#define END_CMD							0xFD
#define SUCCESS_CMD						0xA0
#define ERROR_CMD						0xAF
#define DOOR_STILL_OPEN_CMD				0xA1
//Timeouts
#define HEARTBEAT_TIMEOUT				4000		//4s in ms
#define FW_UPDATE_TIMEOUT				500000 		//10min in ms
#define COMPRESSOR_OFF_TIMEOUT			300000		//5min in ms
#define DEFROST_TIMEOUT					21600000	//6h in ms
#define DEFROST_RUN_TIME				1800000		//30min in ms
// Memory constrains
#define DATA_1							(uint32_t)0x20030000
#define DATA_2							(uint32_t)0x00000000
#define DATA_3							(uint32_t)0x08000000
#define FW_REQUEST_FLAG					(uint32_t)0xBBBBBBBB


#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
