#ifndef ADCMANAGER_H_
#define ADCMANAGER_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Exported functions prototypes ---------------------------------------------*/
uint8_t ADCSensors_Config(void);
void adcStartDMA(void);
void updateAverageValue(void);
uint8_t readCurrentSample(void);
uint32_t readSensor(uint8_t id);

/* Private define ------------------------------------------------------------*/

//###############  ADC SENSORS  ###############
#define RIGHT_WALL_TEMP_Pin 			GPIO_PIN_0
#define RIGHT_WALL_TEMP_GPIO_Port 		GPIOA
#define RIGHT_WALL_TEMP_ID				0
#define RIGHT_WALL_TEMP_ADC_CH			ADC_CHANNEL_0
#define FRIDGE_TEMP_A					0.003354016435
#define FRIDGE_TEMP_B					0.0003001308251
#define FRIDGE_TEMP_C					0.000005085164944
#define FRIDGE_TEMP_D					0.0000002187650493
#define FRIDGE_TEMP_RPU					12.0	//kOhm
#define FRIDGE_TEMP_R25					10.0	//kOhm
//
#define LEFT_WALL_TEMP_Pin 				GPIO_PIN_1
#define LEFT_WALL_TEMP_GPIO_Port 		GPIOA
#define LEFT_WALL_TEMP_ID				1
#define LEFT_WALL_TEMP_ADC_CH			ADC_CHANNEL_1
//
#define PCB_TEMP_Pin 					GPIO_PIN_2
#define PCB_TEMP_GPIO_Port 				GPIOA
#define PCB_TEMP_ID						2
#define PCB_TEMP_ADC_CH					ADC_CHANNEL_2
#define PCB_TEMP_VOFF					0.5
#define PCB_TEMP_M						0.01
//
#define SPARE_1_TEMP_Pin 				GPIO_PIN_3
#define SPARE_1_TEMP_GPIO_Port 			GPIOA
#define SPARE_1_TEMP_ID					3
#define SPARE_1_TEMP_ADC_CH				ADC_CHANNEL_3
//
#define SPARE_2_TEMP_Pin 				GPIO_PIN_4
#define SPARE_2_TEMP_GPIO_Port 			GPIOA
#define SPARE_2_TEMP_ID					4
#define SPARE_2_TEMP_ADC_CH				ADC_CHANNEL_4
//
#define SPARE_3_TEMP_Pin 				GPIO_PIN_5
#define SPARE_3_TEMP_GPIO_Port 			GPIOA
#define SPARE_3_TEMP_ID					5
#define SPARE_3_TEMP_ADC_CH				ADC_CHANNEL_5
//
#define EVAPORATOR_TEMP_Pin 			GPIO_PIN_6
#define EVAPORATOR_TEMP_GPIO_Port 		GPIOA
#define EVAPORATOR_TEMP_ID				6
#define EVAPORATOR_TEMP_ADC_CH			ADC_CHANNEL_6
//
#define DEFROST_HEATER_TEMP_1_Pin 		GPIO_PIN_7
#define DEFROST_HEATER_TEMP_1_GPIO_Port GPIOA
#define DEFROST_HEATER_TEMP_1_ID		7
#define DEFROST_HEATER_TEMP_1_ADC_CH	ADC_CHANNEL_7
//
#define DEFROST_HEATER_TEMP_2_Pin 		GPIO_PIN_4
#define DEFROST_HEATER_TEMP_2_GPIO_Port GPIOC
#define DEFROST_HEATER_TEMP_2_ID		8
#define DEFROST_HEATER_TEMP_2_ADC_CH	ADC_CHANNEL_14
//
#define UC_TEMP_ID						9
#define UC_TEMP_ACD_CH					ADC_CHANNEL_TEMPSENSOR
#define UC_TEMP_VOFF					0.76
#define UC_TEMP_M						0.0025
#define UC_TEMP_TOFF					25

//###############  General configuration  ###############
#define ADC_OFFSET						0x10
#define ADC_NUMBER_SENSORS				10
#define ADC_MAX_VOLT					3.3
#define ADC_MAX_VALUE					4095.0
#define ADC_TBS							2 		//ms
#define ADC_SAMPLES						25
#define MOVING_AVG_SAMPLES				20
#define KELVIN_2_CELSIUS				273.15
//
#define ADC_MODULE						ADC1
#define ADC_CLK_ENABLE					__HAL_RCC_ADC1_CLK_ENABLE
#define ADC_CLK_DISABLE					__HAL_RCC_ADC1_CLK_DISABLE
#define ADC_FORCE_RESET					__HAL_RCC_ADC_FORCE_RESET
#define ADC_RELEASE_RESET				__HAL_RCC_ADC_RELEASE_RESET
#define ADC_IRQn						ADC_IRQn
#define ADC_DMA_CLK_ENABLE				__HAL_RCC_DMA2_CLK_ENABLE
#define ADC_DMA_STREAM_IRQn				DMA2_Stream0_IRQn
#define ADC_DMA_IRQn_HANDLER			DMA2_Stream0_IRQHandler
#define ADC_DMA_STREAM					DMA2_Stream0
#define ADC_DMA_CHANNEL					DMA_CHANNEL_0

#endif /* ADCMANAGER_H_ */

