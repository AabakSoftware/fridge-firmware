#ifndef I2CMANAGER_H_
#define I2CMANAGER_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* External functions --------------------------------------------------------*/
uint8_t i2cConfig(void);
//LCD control
uint8_t lcdConfig(void);
uint8_t lcdClear(void);
uint8_t lcdResetCursor(void);
uint8_t lcdDisplayOn(void);
uint8_t lcdDisplayOff(void);
uint8_t lcdDisplayWrite(uint8_t* data, uint8_t length);
uint8_t lcdSetCursor(uint8_t pos, uint8_t row);
uint8_t lcdDisplayTemp(uint32_t targetTemp, float averageTemp, uint8_t mode, uint8_t status, uint8_t step);
//Humidity sensor control
uint8_t humiditySensorConfig(void);
uint8_t humiditySensorEnableHeater(void);
uint8_t humiditySensorDisableHeater(void);
uint8_t humiditySensorSequentialReadConfig(void);
uint8_t humiditySensorSerialReadConfig(void);
uint8_t humiditySensorRequestTempRead(void);
uint8_t humiditySensorRequestHumidityRead(void);
uint32_t humiditySensorReadSerialValue(void);
uint32_t humiditySensorReadSequentialValue(void);
//Frost sensor control
uint8_t frostSensorConfig(void);
uint32_t frostSensorReadValue(uint8_t sensorID);

/* External defines ----------------------------------------------------------*/
//###############  I2C communication pins  ###############
#define I2C_SCL_Pin						GPIO_PIN_6
#define I2C_SCL_GPIO_Port				GPIOB
#define I2C_SDA_Pin						GPIO_PIN_7
#define I2C_SDA_GPIO_Port				GPIOB

//###############  I2C Addresses  ###############
#define I2C_MASTER_ADDR					0x11
//LDC configuration
//#######
#define LCD_ADDRESS     				0x7C
//
#define LCD_DISPLAY_CLEAR				0x01
#define LCD_CURSOR_RETURN				0x02
//
#define LCD_SET_ENTRY_CONFIG			0x04
#define LCD_ENTRY_RIGHT					0x00
#define LCD_ENTRY_LEFT					0x02
#define LCD_ENTRY_INCREMENT				0x01
#define LCD_ENTRY_DECREMENT				0x00
//
#define LCD_SET_DISPLAY_CONFIG			0x08
#define LCD_BACKLIGHT_BLINK_ON			0x01
#define LCD_BACKLIGHT_BLINK_OFF			0x00
#define LCD_CURSOR_ON					0x02
#define LCD_CURSOR_OFF					0x00
#define LCD_DISPLAY_ON					0x04
#define LCD_DISPLAY_OFF					0x00
//
#define LCD_SET_DISPLAY_SH_CONFIG		0x10
#define LCD_DISPLAY_SHIFT				0x08
#define LCD_CURSOR_SHIFT				0x00
#define LCD_RIGHT_SHIFT					0x04
#define LCD_LEFT_SHIFT					0x00
//
#define LCD_SET_DISPLAY_FUNC_CONFIG		0x20
#define LCD_1_LINE_MODE					0x00
#define LCD_2_LINE_MODE					0x08
#define LCD_5_8_STYLE_MODE				0x00
#define LCD_5_11_STYLE_MODE				0x04

//Humidity sensor configuration
//######
#define HDC1080_WRITE_ADDR				0x80
#define HDC1080_READ_ADDR				0x81
#define HDC1080_TEMP_REG				0x00
#define HDC1080_HUMIDITY_REG			0x01
#define HDC1080_CONFIG_REG				0x02
//
#define HDC1080_CONFIG_B1				0x00	//RST,r,HEAT,MODE,BTST,TRES,HRES[1],HRES[0]
#define HDC1080_CONFIG_B0				0x00	//r,r,r,r,r,r,r,r
//
#define HDC1080_HRES_14BITS				0x00
#define HDC1080_HRES_11BITS				0x01
#define HDC1080_HRES_8BITS				0x10
#define HDC1080_TRES_14BITS				0x00
#define HDC1080_TRES_11BITS				0x04
#define HDC1080_BTST_HV					0x00
#define HDC1080_BTST_LV					0x08
#define HDC1080_SERIAL_READ_MODE		0x00
#define HDC1080_SEQUENTIAL_READ_MODE	0x10
#define HDC1080_HEATER_DIS				0x00
#define HDC1080_HEATER_EN				0x20
#define HDC1080_RST						0x80
//
#define HDC1080_READ_TIMEOUT			100

//Capacitance reader configuration
//######
#define FDC2212_WRITE_ADDR				0x56 	//0x2B<<1 = 0x56
#define FDC2212_READ_ADDR				0x57	//0x2B<<1 + 1 = 0x57
#define FDC2212_DATA_CH0_MSB_REG		0x00
#define FDC2212_DATA_CH0_LSB_REG		0x01
#define FDC2212_DATA_CH1_MSB_REG		0x02
#define FDC2212_DATA_CH1_LSB_REG		0x03
#define FDC2212_RCOUNT_CH0_REG			0x08	//Reference count setting for ch 0 default value 0x0080
#define FDC2212_RCOUNT_CH1_REG			0x09
#define FDC2212_OFFSET_CH0_REG			0x0C
#define FDC2212_OFFSET_CH1_REG			0x0D
#define FDC2212_SETTLECOUNT_CH0_REG		0x10	//Ch 0 settling reference count
#define FDC2212_SETTLECOUNT_CH1_REG		0x11
#define FDC2212_CLKDIV_CH0_REG			0x14	//Reference clock dividers for ch 0
#define FDC2212_CLKDIV_CH1_REG			0x15
#define FDC2212_STATUS_REG				0x18	//Device status reporting
#define FDC2212_STATUS_CONFIG_REG		0x19
#define FDC2212_CONFIG_REG				0x1A	//Conversion configuration, default value 0x2801
#define FDC2212_MUX_CONFIG_REG			0x1B	//Channel multiplexing configuration, default  value 0x020f
#define FDC2212_RST_REG					0x1C
#define FDC2212_DRIVECURRENT_CH0_REG	0x1E	//Ch 0 sensor current drive configuration
#define FDC2212_DRIVECURRENT_CH1_REG	0x1F
#define FDC2212_MANUFACTURER_ID_REG		0x7E	//default value 0x5449
#define FDC2212_DEVICE_ID_REG			0x7F	//default value 0x3055
#define FDC2212_DEVICE_ID				0x3055
//
#define FDC2212_L						0.0000086		//H
#define FDC2212_C						0.000000000039	//F
#define FDC2212_COUNT					0x2089
#define FDC2212_FREF					40000000.0	//Hz
#define FDC2212_CH_FIN_SELECT			0x02
#define PI								3.14159265
#define CAP_SENSOR_1_ID					0
#define CAP_SENSOR_2_ID					1
#define CAP_SENSORS_NUMBER				2

//###############  I2C Buffer features  ###############
#define I2C_READ_BUFFER_LENGTH			4
#define I2C_WRITE_BUFFER_LENGTH			2

//###############  I2C General configuration  ###############
#define I2Cx							I2C1
#define I2Cx_CLK_ENABLE					__HAL_RCC_I2C1_CLK_ENABLE
#define I2Cx_CLK_DISABLE				__HAL_RCC_I2C1_CLK_DISABLE
#define I2Cx_GPIO_AF					GPIO_AF4_I2C1
#define I2Cx_ER_IRQn					I2C1_ER_IRQn
#define I2Cx_EV_IRQn					I2C1_EV_IRQn
#define I2Cx_ER_IRQHandler				I2C1_ER_IRQHandler
#define I2Cx_EV_IRQHandler				I2C1_EV_IRQHandler

#endif /* I2CMANAGER_H_ */

